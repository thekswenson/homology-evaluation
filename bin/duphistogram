#!/usr/bin/env python3
"""
Evaluate the number of copies of markers in marker-order data.
"""

import argparse
import csv
from collections import Counter, defaultdict, namedtuple
from statistics import mean
from pathlib import Path, PurePath
import glob
from typing import Dict, List, Optional, Set, Tuple

from pybedtools.bedtool import BedTool
from homology_evaluation.utilities import mymean, mystdev


#Switches:

#Constants:

#______________________________________________________________________________#
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|   OBJECTS   |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#

MarkerInfo = namedtuple('MarkerInfo', ['marker', 'mean', 'dev', 'length'])

#______________________________________________________________________________#
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|  FUNCTIONS  |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#

def writeCSV(csvoutfile, allgeneinfos: List[List[MarkerInfo]], filenames,
             remname, remdev, addlength):
  with open(csvoutfile, 'w', newline='') as csvfile:
    w = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_NONNUMERIC)

    numentries = 3 - (1 if remname else 0) - (1 if remdev else 0) + \
                 (1 if addlength else 0)
    w.writerow(formatHeader(filenames, numentries-1))

    for gilist in zip(*allgeneinfos):
      row = []
      for gi in gilist:
        if not remname:
          row.append(gi.marker)

        row.append(f'{gi.mean:.2f}')

        if not remdev:
          row.append(f'{gi.dev:.2f}')
        if addlength:
          row.append(f'{gi.length:.2f}')

      w.writerow(row)
    

def formatHeader(l, numspaces=1):
  """
  Add spaces after every entry in the list.
  """
  return [val for entry in l for val in [entry]+['']*numspaces]


def chooseBEDFile(name, dir):
  """
  Find the BED file with the matching name.
  """
  if not dir:
    return ''
  
  files = glob.glob(str(PurePath(dir, name+'.bed*')))
  if not files:
    raise(Exception(f'No matching BED file found for {PurePath(dir, name)}'))
  elif len(files) > 1:
    raise(Exception(f'Too many matching BED files: {files}'))
  else:
    return files[0]


def getMarkerLengths(markers: Set[int], intervals: Optional[BedTool]) \
  -> Dict[int, float]:
  """
  Map marker name to interval length.

  Parameters
  ----------
  markers : Set[int]
      the set of markers of interest
  intervals : BedTool
      the intervals

  Returns
  -------
  Dict[int, float]
      a map from block name to block length
  """
  if not intervals:
    return None

  block2lens = defaultdict(list)
  for interval in intervals:
    if int(interval.name) in markers:
      block2lens[int(interval.name)].append(len(interval))

  return {name: mean(lens) for name, lens in block2lens.items()}


def readMarkerCounts(filename,
                     outpositions: int=0) -> Tuple[Dict[int, List[int]], int]:
  """
  From the given fasta file, count the number of times each marker occurs.

  Notes
  -----
      - The fasta file is assumed to have a genome on every-other line
      (in Bielefeld UNIMOG format)!
      - Integer markers are assumed.

  Parameters
  ----------
  filename : str
      the fasta file
  outpositions : int, optional
      print the marker counts for this many genomes, by default False

  Returns
  -------
  Tuple[Dict[int, List[int]], int]
      (marker2counts, numgenomes) where marker2counts maps the marker name to a
      list of the number of occurrences in the genomes, and numgenomes is the
      number of genomes in the input fasta file
  """
  with open(filename) as f:
    lines = f.readlines()

  numgenomes = 0
  marker2counts: Dict[int, List[int]] = defaultdict(list)
  for i in range(1, len(lines), 2):         #assume genome on every other line
    genecounts: Dict[int, int] = Counter()
    numgenomes += 1
    for marker in lines[i].split():
      try:
          genenum = abs(int(marker))
          genecounts[genenum] += 1
      except:
          pass                              #hack to ignore the ')' or '$' :(

    for genenum, count in genecounts.items():
      marker2counts[genenum].append(count)
  
    if outpositions:
      print(f'{lines[i-1].strip()} {sorted(genecounts.values())[(-1*outpositions):]}')

  return marker2counts, numgenomes


def mapMarker2Info(marker2counts, numgenomes, numpositions=10,
                   lengthfile='') -> List[MarkerInfo]:
  """
  Return MarkerInfo for the `numpositions` most frequent markers.

  Parameters
  ----------
  marker2counts : Dict[int, List[int]]
      map marker to list of number of occurrences
  numgenomes : int
      the number of genomes
  numpositions : int
      report this many of the most frequent markers
  lengthfile : str
      get marker length from this file
  """
  sortedcounts = sorted(marker2counts.items(),
                        reverse=True,
                        key=lambda t: mymean(t[1], numgenomes))[:numpositions]

  marker2len = {}                     #Map marker to marker length
  if lengthfile:
    try:
      intervals = BedTool(lengthfile)
    except Exception as e:            #missing BED file
      print(f'WARNING: {e}')
      intervals = None
      pass

    marker2len = getMarkerLengths(set(marker for marker, _ in sortedcounts), intervals)

  markerinfos: List[MarkerInfo] = []                #List of (marker, avg, dev)
  for marker, counts in sortedcounts:
    length = -2.0
    if lengthfile:
      length = marker2len[marker]

    markerinfos.append(MarkerInfo(marker, mymean(counts, numgenomes),
                                  mystdev(counts, numgenomes), length))

  return markerinfos


#______________________________________________________________________________#
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|     Main    |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#

#Preliminaries: __    __    __    __    __    __    __    __    __    __    __
#__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \_

def main():
  desc = 'Show the frequency of each marker in the given marker orders.'
  parser = argparse.ArgumentParser(description=desc)

  parser.add_argument('FASTAFILE', nargs='+',
                      help='the fasta filenames (assume every-other line has marker sequence in UniMoG format).')
  parser.add_argument('-n', metavar='NUM_POSITIONS', default=10, type=int,
                      help='show the NUM_POSITIONS most significant dup counts (default 10)')
  parser.add_argument('-c', '--csv-output', metavar='FILE_NAME',
                      help='output to this CSV file')
  parser.add_argument('--name-ignore', action='store_true',
                      help='do not include the marker names column in the csv')
  parser.add_argument('--dev-ignore', action='store_true',
                      help='do not include the stddev column in the csv')
  parser.add_argument('-p', '--per-genome', action='store_true',
                      help='show details per genome')
  parser.add_argument('-l', '--lengths', metavar='BED_DIR',
                      help='get the marker lengths from this directory (the name must match that fasta filename)')

  args = parser.parse_args()

  fasta_files = args.FASTAFILE
  num_positions = args.n
  per_genome = args.per_genome
  csv_outfile = args.csv_output
  ignore_names = args.name_ignore
  ignore_devs = args.dev_ignore
  get_lengths = args.lengths

  if csv_outfile and Path(csv_outfile).exists():
    exit(f'The csv file "{csv_outfile}" exists!')

  if get_lengths and not Path(get_lengths).is_dir():
    exit(f'The lengths directory "{get_lengths}" does not exist!')

#MAIN:    __    __    __    __    __    __    __    __    __    __    __    __
#__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \_

  filenames = []
  allgeneinfos: List[List[MarkerInfo]] = []
  for filename in fasta_files:
    name = Path(filename).stem

      #Read the genes from the file:
    marker2counts, numgenomes = readMarkerCounts(filename,
                                                 per_genome and num_positions)
      #Organize the genes by frequency:
    markerinfos = mapMarker2Info(marker2counts, numgenomes, num_positions,
                                 chooseBEDFile(name, get_lengths))

      #Print the output and retreive the lengths:
    print(f'{name}: ave  dev  len')
    for marker, freqave, freqdev, length in markerinfos:
      if not get_lengths:
        length = -0.0

      print(f'{marker}\t{freqave:.2f}\t{freqdev:.2f}\t{length:.2f}')

    if csv_outfile:
      filenames.append(name)
      allgeneinfos.append(markerinfos)

  if csv_outfile:
    writeCSV(csv_outfile, allgeneinfos, filenames, ignore_names,
             ignore_devs, get_lengths)

if __name__ == "__main__":
  #import doctest  
  #doctest.testmod()
  main()                                           
