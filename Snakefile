# coding: utf-8

import shutil
import pandas as pd

from pathlib import Path
from itertools import combinations

from homology_evaluation.plots import makeCutsetHeatmap, makeSummaryPlot
from homology_evaluation.plots import makeBigCoordsPlot, makeBigCoordsPlotPerGenome


#### BASIC CONSTANTS ####
configfile: 'snakeconfig/default.yaml'

EVALBLOCKS = 'bin/evalblocks'
SRCDIR = srcdir('src')
#EXPORT_PYTHONPATH = f'export PYTHONPATH="$PYTHONPATH:{SRCDIR}"'
EXPORT_PYTHONPATH = f'export PYTHONPATH="{SRCDIR}"'
BED_EXT = '.bed.gz'
MAF_EXT = '.maf.gz'
#DEF_PREF = 'evalblocks'
COMPONENTS_DIR = 'components'
BIGCOORDS_DIR = 'bigcoords'
CUTSETS_DIR = 'cutsets'
DETAILS_DIR = 'details'   #The subdirectory under which we put the details
BIGCOORDS_PLOT_EXT = '.png'
BIGCOORDS_PER_COMP = False  #NOTE: make a config file parameter for this

#### CONFIG VALUES ####

  #Required values
OUT_DIR = str(Path(config['OUT_DIR']))       #Can be modified with STRIP_LEN
IN_DIR = str(Path(config['IN_DIR']))
GRAPHS_DIR = str(Path(config['GRAPHS_DIR'])) #Can be modified with STRIP_LEN

  #Optional values
STRIP_LEN = int(config['STRIP_LEN']) if 'STRIP_LEN' in config else 0
if STRIP_LEN:                                #Add a suffix with STRIP_LEN
  OUT_DIR += f'_{STRIP_LEN}'
  GRAPHS_DIR += f'_{STRIP_LEN}'

MAX_PROCESSORS = int(config['MAX_PROC']) if 'MAX_PROC' in config else 1
SPECIFIC_PAIRS = config['SPECIFIC_PAIRS'] if 'SPECIFIC_PAIRS' in config else []
COMBINED_CSV = str(Path(OUT_DIR, config['COMBINED_CSV'])) if 'COMBINED_CSV' in config else ''
HEATMAP = str(Path(OUT_DIR, config['HEATMAP'])) if 'HEATMAP' in config else ''
SUMMARY_PLOT = str(Path(OUT_DIR, config['SUMMARY_PLOT'])) if 'SUMMARY_PLOT' in config else ''
MAKE_BIGCOORDS_PER = config['MAKE_BIGCOORDS_PER'] if 'MAKE_BIGCOORDS_PER' in config else False
BIGCOORDS_PREF = config['BIGCOORDS_PREF'] if 'BIGCOORDS_PREF' in config else ''
SUMMARY_DIR = str(Path(OUT_DIR, config['SUMMARY_DIR'])) if 'SUMMARY_DIR' in config else ''
DRAW_COMPONENTS = config['DRAW_COMPONENTS'] if 'DRAW_COMPONENTS' in config else True
SAVE_CUTSETINFO = config['SAVE_CUTSETINFO'] if 'SAVE_CUTSETINFO' in config else True
NAME_MAP = config['NAME_MAP'] if 'NAME_MAP' in config else None

if 'GET_IGNORESET' not in config:
  IGNORE_SET = ''
elif config['GET_IGNORESET']:
  IGNORE_SET = '--ignoreset'


def buildTargetList(wildcards):
  """
  Build the list of ultimate targets based on the current set of constants.
  """
  #if COMBINED_CSV:
  #  return COMBINED_CSV

  filelist = buildPairList(wildcards) + [COMBINED_CSV] #+ buildDatasetLinkTargetList(wildcards)
  if HEATMAP:
    filelist.append(HEATMAP)
  if SUMMARY_PLOT:
    filelist.append(SUMMARY_PLOT)
  if BIGCOORDS_PREF:
    filelist.append(f'{OUT_DIR}/bigcoords/{BIGCOORDS_PREF}/')

  return filelist


def buildGraphTargetList(wildcards):
  """
  Build the list of targets so that only the graph files are computed.
  """
  return buildPairList(wildcards, True)


def buildComponentTargetList(wildcards):
  """
  Build the list of targets so that only the component figures are created.
  """
  filelist = []
  if SPECIFIC_PAIRS:
    for i, j in SPECIFIC_PAIRS:
      filelist.append(f'{OUT_DIR}/{DETAILS_DIR}/{i}_VS_{j}/{COMPONENTS_DIR}')

  else:
      filelist += addAllPairs(IN_DIR, OUT_DIR, compdirsonly=True)

  return filelist


def buildCutsetTargetList(wildcards):
  """
  Build the list of targets so that only the component figures are created.
  """
  filelist = []
  if SPECIFIC_PAIRS:
    for i, j in SPECIFIC_PAIRS:
      filelist.append(f'{OUT_DIR}/{DETAILS_DIR}/{i}_VS_{j}/{CUTSETS_DIR}')
  else:
      filelist += addAllPairs(IN_DIR, OUT_DIR, cutsetdirsonly=True)

  return filelist


def buildBigCoordsPerComponentTargetList(wildcards):
  """
  Build the list of targets for the bigcoords plots per component.
  """
  dirlist = []
  if SPECIFIC_PAIRS:
    for i, j in SPECIFIC_PAIRS:
      dirlist.append(f'{OUT_DIR}/{DETAILS_DIR}/{i}_VS_{j}/{BIGCOORDS_DIR}')
  else:
      dirlist += addAllPairs(IN_DIR, OUT_DIR, bigcoordsdironly=True)

  filelist = []
  for d in dirlist:
    indices = glob_wildcards(d+'/bigcoords_{i}.csv.gzip').i
    filelist += expand(d+'/bigcoords_{i}'+BIGCOORDS_PLOT_EXT, i=indices)

  return filelist


def buildPairList(wildcards, graphsonly=False, outcsvonly=False,
                  bigcoordscsvonly=False):
  """
  Build a list of output files for all pairs of datasets we will compute.
  """
  filelist = []
  if SPECIFIC_PAIRS:
    for i, j in SPECIFIC_PAIRS:
      if graphsonly:
        filelist.append(f'{GRAPHS_DIR}/{i}_VS_{j}_graph.bz2',)
      elif bigcoordscsvonly:
        filelist.append(f'{OUT_DIR}/{DETAILS_DIR}/{i}_VS_{j}/{i}_VS_{j}_bigcoords.csv.gzip')
      else:
        filelist.append(f'{OUT_DIR}/{DETAILS_DIR}/{i}_VS_{j}/{i}_VS_{j}_output.csv')
        if not outcsvonly and MAKE_BIGCOORDS_PER:
          filelist.append(f'{OUT_DIR}/{DETAILS_DIR}/{i}_VS_{j}/{i}_VS_{j}_bigcoords{BIGCOORDS_PLOT_EXT}')

  else:
      filelist += addAllPairs(IN_DIR, OUT_DIR, graphsonly, outcsvonly=outcsvonly,
                              bigcoordscsvonly=bigcoordscsvonly)

  return filelist


def buildCSVList(wildcards):
  """
  Return a list of the CSVs.
  """
  return buildPairList(wildcards, outcsvonly=True)


def buildBigCoordsCSVList(wildcards):
  """
  Return a list of the CSVs containing coordinates from big components.
  """
  return buildPairList(wildcards, bigcoordscsvonly=True)


def addAllPairs(indir, outdir, graphsonly=False, compdirsonly=False,
                dirlinksonly=False, cutsetdirsonly=False, outcsvonly=False,
                bigcoordscsvonly=False, bigcoordsdironly=False):
  """
  For all pairs of input files, return the output files to create.
  """
  filelist = []

  names, = glob_wildcards(indir+'/{name}'+BED_EXT)
  mafnames, = glob_wildcards(indir+'/{name}'+MAF_EXT)
  names += mafnames

  for i, j in combinations(sorted(names), 2):
    if graphsonly:
      filelist.append(f'{GRAPHS_DIR}/{i}_VS_{j}_graph.bz2',)
    elif compdirsonly:
      filelist.append(f'{outdir}/{DETAILS_DIR}/{i}_VS_{j}/{COMPONENTS_DIR}')
    elif cutsetdirsonly:
      filelist.append(f'{outdir}/{DETAILS_DIR}/{i}_VS_{j}/{CUTSETS_DIR}')
    elif dirlinksonly:
      filelist.append(f'{SUMMARY_DIR}/{i}_VS_{j}_link')
    elif bigcoordscsvonly:
      filelist.append(f'{OUT_DIR}/{DETAILS_DIR}/{i}_VS_{j}/{i}_VS_{j}_bigcoords.csv.gzip')
    elif bigcoordsdironly:
      filelist.append(f'{OUT_DIR}/{DETAILS_DIR}/{i}_VS_{j}/{BIGCOORDS_DIR}')
    else:
      #filelist.append(f'{SUMMARY_DIR}/{i}_VS_{j}_link')
      filelist.append(f'{outdir}/{DETAILS_DIR}/{i}_VS_{j}/{i}_VS_{j}_output.csv')
      if not outcsvonly and MAKE_BIGCOORDS_PER:
        filelist.append(f'{outdir}/{DETAILS_DIR}/{i}_VS_{j}/{i}_VS_{j}_bigcoords{BIGCOORDS_PLOT_EXT}')

  return filelist


def buildDatasetLinkTargetList(wildcards):
  """
  Build the list of targets so that the links to the datasets are created.
  """
  filelist = []
  if SPECIFIC_PAIRS:
    for i, j in SPECIFIC_PAIRS:
      filelist.append(f'{SUMMARY_DIR}/{i}_VS_{j}_link')

  else:
      filelist += addAllPairs(IN_DIR, OUT_DIR, dirlinksonly=True)

  return filelist


def removeComponentTargetList(wildcards):
  """
  Remove the component directories where the figures are created.
  """
  filelist = buildComponentTargetList(wildcards)

  print(f'Will remove these files:')
  for f in filelist:
    if Path(f).exists():
      print(f'  {f}')
  sure = input('Are you sure (Y/n)? ')

  yes = 'yes'
  if sure == '' or sure == yes[:len(sure)]:
    for f in filelist:
      shutil.rmtree(f, ignore_errors=True)

  return []


def removeCutsetTargetList(wildcards):
  """
  Remove the component directories where the figures are created.
  """
  filelist = buildCutsetTargetList(wildcards)

  print(f'Will remove these files:')
  for f in filelist:
    if Path(f).exists():
      print(f'  {f}')
  sure = input('Are you sure (Y/n)? ')

  yes = 'yes'
  if sure == '' or sure == yes[:len(sure)]:
    for f in filelist:
      shutil.rmtree(f, ignore_errors=True)

  return []


def removeAllTargetList(wildcards):
  """
  Remove the final targets for the rule all.
  """
  filelist = buildTargetList(wildcards)

  print(f'Will remove these files:')
  for f in filelist:
    if Path(f).exists():
      print(f'  {f}')
  sure = input('Are you sure (Y/n)? ')

  yes = 'yes'
  if sure == '' or sure == yes[:len(sure)]:
    for f in filelist:
      Path(f).unlink(missing_ok=True)

  return []


#-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-
# Targets
#-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-


rule all:
  input:
    buildTargetList


rule rm_all_targets:
  """
  Delete the targets from the all rule.
  """
  input:
    removeAllTargetList


rule graph_targets:
  """
  Only build the graph.bz2 files, without analyzing them.
  """
  input:
    buildGraphTargetList


rule component_targets:
  """
  Rerun process_graph soas to draw the component figures.
  """
  input:
    buildComponentTargetList

rule bigcoords_target:
  """
  Create a coordinates plot per big component.
  """
  input:
    buildBigCoordsPerComponentTargetList

rule rm_component_targets:
  """
  Delete the component figures directories.
  """
  input:
    removeComponentTargetList


rule cutset_targets:
  """
  Rerun process_graph soas to compute info about the cutsets.
  """
  input:
    buildCutsetTargetList


rule rm_cutset_targets:
  """
  Delete the cutset directories.
  """
  input:
    removeCutsetTargetList


rule dataset_link_targets:
  """
  Create a directory with symbolic links for a dataset.
  """
  input:
    buildDatasetLinkTargetList


#-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-
# Plots
#-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-


rule make_cutset_heatmap:
  """
  Given a .csv file summarizing a dataset, create a heatmap showing the cut
  set costs.
  """
  input:
    '{outdir}/{setname}.csv',

  output:
    '{outdir}/{setname}_heatmap.pdf',

  run:
    makeCutsetHeatmap(str(input), str(output), NAME_MAP)


if SUMMARY_PLOT:
  rule make_summary_plot:
    """
    Given a .csv file summarizing a dataset, create a plot.
    """
    input:
      summaryfile = '{outdir}/{setname}.csv',
      lengthsfile = '{outdir}/genome_lengths.csv',

    output:
      '{outdir}/{setname}_summary.pdf',

    run:
      makeSummaryPlot(str(input.summaryfile), str(input.lengthsfile), str(output))


rule make_bigcoordinates_percomponent_plot:
  """
  Given a .csv file containing the coordinates for a single big connected
  component of the block graph, create a plot showing the coordinates.
  """
  input:
    '{outdir}/'+BIGCOORDS_DIR+'/bigcoords_{i}.csv.gzip',

  output:
    '{outdir}/'+BIGCOORDS_DIR+'/bigcoords_{i}'+BIGCOORDS_PLOT_EXT,

  run:
    makeBigCoordsPlot(str(input), str(output))


rule make_bigcoordinates_perpair_plot:
  """
  Given a .csv file containing the coordinates for the big connected
  components of the block graph, create a plot showing the coordinates.
  """
  input:
    '{outdir}/{pair}/{pair}_bigcoords.csv.gzip',

  output:
    '{outdir}/{pair}/{pair}_bigcoords'+BIGCOORDS_PLOT_EXT,

  run:
    makeBigCoordsPlot(str(input), str(output))


rule make_bigcoordinates_pergenome_plot:
  """
  Given a .csv file containing the coordinates for the big connected
  components of many block graphs, create a plot showing the coordinates
  for each genome.
  """
  input:
    coordsfile = OUT_DIR+'/bigcoords/{setname}_bigcoords.csv.gzip',
    lengthsfile = OUT_DIR+'/genome_lengths.csv',

  output:
    directory(OUT_DIR+'/bigcoords/{setname}/')

  run:
    makeBigCoordsPlotPerGenome(str(input.coordsfile), str(input.lengthsfile),
                               str(output), BIGCOORDS_PLOT_EXT)


#-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-
# Other Rules
#-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-


rule combine_summary_csv:
  """
  Combine the CSVs for a dataset into a single CSV.
  """
  input:
    buildCSVList,

  output:
    COMBINED_CSV,

  run:
    infiles = ' '.join(f"'{filename}'" for filename in input)
    shell(f'cat {infiles} > {output}')


rule combine_bigcoords_csv:
  """
  Combine the CSVs for a dataset into a single CSV.
  """
  input:
    buildBigCoordsCSVList

  output:
    OUT_DIR+'/bigcoords/'+BIGCOORDS_PREF+'_bigcoords.csv.gzip',

  run:
    dfs = [pd.read_csv(filename) for filename in input]
    pd.concat(dfs, ignore_index=True).to_csv(str(output), index=False)


#rule rename_graphs:
#  """
#  Process the blocks and output the graph file for `process_graph`
#  """
#  input:
#    ancient(GRAPHS_DIR+'/{bed2}_VS_{bed1}_graph.bz2'),
#
#  output:
#    GRAPHS_DIR+'/{bed1}_VS_{bed2}_graph.bz2',
#
#  shell:
#    "mv '{input}' '{output}'"

def blocks_to_graphs_Input1(wildcards):
  maf = Path(IN_DIR, wildcards.bed1+MAF_EXT)
  if maf.exists():
    return str(maf)
  else:
    return str(Path(IN_DIR, wildcards.bed1+BED_EXT))


def blocks_to_graphs_Input2(wildcards):
  maf = Path(IN_DIR, wildcards.bed2+MAF_EXT)
  if maf.exists():
    return str(maf)
  else:
    return str(Path(IN_DIR, wildcards.bed2+BED_EXT))


rule blocks_to_graphs:
  """
  Process the blocks and output the graph file for `process_graph`
  """
  input:
    input1 = blocks_to_graphs_Input1,
    input2 = blocks_to_graphs_Input2,

  output:
    outfile = OUT_DIR+'/'+DETAILS_DIR+'/{bed1}_VS_{bed2}/{bed1}_VS_{bed2}_blocks.txt',
    graphfile = GRAPHS_DIR+'/{bed1}_VS_{bed2}_graph.bz2',

  log:
    OUT_DIR+'/'+DETAILS_DIR+'/{bed1}_VS_{bed2}/{bed1}_VS_{bed2}_blockslog.txt',

  threads: 1

  run:
    strip1 = ''
    strip2 = ''
    if STRIP_LEN:
      strip1 = f'--strip1 {STRIP_LEN}'
      strip2 = f'--strip2 {STRIP_LEN}'

    shell("{EXPORT_PYTHONPATH}; "
          "{EVALBLOCKS} -t {threads} blockfiles -s {strip1} {strip2} "
          "--graph-file '{output.graphfile}' "
          "'{input.input1}' '{input.input2}' > '{output.outfile}' 2> '{log}'")


rule process_graph:
  """
  Given a graph.bz2 file, create the .csv file and analyze the connected
  components.
  """
  input:
    GRAPHS_DIR+'/{set1}_VS_{set2}_graph.bz2',

  output:
    graphout = '{outdir}/{set1}_VS_{set2}/{set1}_VS_{set2}_graph.txt',
    csvfile = '{outdir}/{set1}_VS_{set2}/{set1}_VS_{set2}_output.csv',
    coordsfile = '{outdir}/{set1}_VS_{set2}/{set1}_VS_{set2}_bigcoords.csv.gzip',
    #coordsdir = directory('{outdir}/{set1}_VS_{set2}/bigcoords'),

  log:
    '{outdir}/{set1}_VS_{set2}/{set1}_VS_{set2}_graphlog.txt',

  threads: 1

  run:
    #olddir = Path(f'{OUT_DIR}/{wildcards.set2}_VS_{wildcards.set1}')
    #if olddir.exists():                      #NOTE: temporary!
    #  print(f'----- Moving from {olddir}!')
    #  prefix = f'{olddir}/{wildcards.set2}_VS_{wildcards.set1}'
    #  toprefix = f'{wildcards.outdir}/{wildcards.set1}_VS_{wildcards.set2}'
    #  Path(f'{prefix}_graph.txt').rename(f'{toprefix}_graph.txt')
    #  Path(f'{prefix}_output.csv').rename(f'{toprefix}_output.csv')
    #  Path(f'{prefix}_blocks.txt').rename(f'{toprefix}_blocks.txt')
    #  Path(f'{prefix}_blockslog.txt').rename(f'{toprefix}_blockslog.txt')
    #  Path(f'{prefix}_graphlog.txt').rename(f'{toprefix}_graphlog.txt')
    #  Path(f'{olddir}/components').rename(f'{wildcards.outdir}/components')
    #  olddir.rmdir()

    #else:
    #  print(f'----- Computing new files.')

    compstr = ''
    d =  f'{wildcards.outdir}/{wildcards.set1}_VS_{wildcards.set2}'
    if DRAW_COMPONENTS:
      cdir = f'{d}/{COMPONENTS_DIR}'
      if Path(cdir).is_dir():
        shutil.rmtree(cdir)

      compstr = f"--allcomponent-files '{cdir}/comp' "

    cutsetstr = ''
    if SAVE_CUTSETINFO:
      cdir = f'{d}/{CUTSETS_DIR}'
      if Path(cdir).is_dir():
        shutil.rmtree(cdir)

      cutsetstr = f"--allcutset-info '{cdir}/cutset' "

    bgper = ''
    if BIGCOORDS_PER_COMP:
      coordsdir = f'{wildcards.outdir}/{wildcards.set1}_VS_{wildcards.set2}/bigcoords'
      bgper = f"--bg-per '{coordsdir}'"

    shell("{EXPORT_PYTHONPATH}; "
          "{EVALBLOCKS} -t {threads} graph {compstr} -o '{output.csvfile}' "
          "-b '{output.coordsfile}' {bgper} {IGNORE_SET} "
          "{cutsetstr} '{input}' > '{output.graphout}' 2> '{log}'")


rule create_components:
  """
  Given a graph.bz2 file, draw the the big components.
  """
  input:
    GRAPHS_DIR+'/{set1}_VS_{set2}_graph.bz2',

  output:
    directory(OUT_DIR+'/'+DETAILS_DIR+'/{set1}_VS_{set2}/'+COMPONENTS_DIR),

  log:
    OUT_DIR+'/'+DETAILS_DIR+'/{set1}_VS_{set2}/{set1}_VS_{set2}_componentlog.txt',

  threads: 1

  run:
    shell("{EXPORT_PYTHONPATH}; "
          "{EVALBLOCKS} graph {IGNORE_SET} --allcomponent-files '{output}/component' "
          "'{input}' &> '{log}'")


rule create_cutsetinfo:
  """
  Given a graph.bz2 file, create a directory containing information about the
  neighborhood of the cutsets.
  """
  input:
    GRAPHS_DIR+'/{set1}_VS_{set2}_graph.bz2',

  output:
    directory(OUT_DIR+'/'+DETAILS_DIR+'/{set1}_VS_{set2}/'+CUTSETS_DIR),

  log:
    OUT_DIR+'/'+DETAILS_DIR+'/{set1}_VS_{set2}/{set1}_VS_{set2}_cutsetlog.txt',

  threads: 1

  run:
    shell("{EXPORT_PYTHONPATH}; "
          "{EVALBLOCKS} graph --allcutset-info '{output}/cutset' "
          "'{input}' &> '{log}'")


rule create_link:
  """
  Create a directory for each dataset.
  """
  input:
    OUT_DIR+'/'+DETAILS_DIR+'/{set1}_VS_{set2}/'
    #OUT_DIR+'/'+DETAILS_DIR+'/{set1}_VS_{set2}/{set1}_VS_{set2}_output.csv',

  output:
    directory(SUMMARY_DIR+'/{set1}_VS_{set2}_link')

  run:
    Path(str(output)).symlink_to(f'../../{input}')


rule copy_genome_lengths:
  input:
    IN_DIR+'/../genome_lengths.csv'

  output:
    OUT_DIR+'/genome_lengths.csv'

  shell:
    "cp '{input}' '{output}'"
