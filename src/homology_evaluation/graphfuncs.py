import re
import math
from itertools import product, chain
from collections import Counter, defaultdict
from typing import Collection, Dict, Iterable, Iterator, List, Optional, Set, Tuple
from pathlib import Path
from prettytable import PrettyTable, NONE
from multiprocessing import Pool

import networkx as nx
from networkx.algorithms import tree

#import matplotlib
#matplotlib.use('Agg')     #To get rid of strange warning
import matplotlib.pyplot as plt

from . import globals as gl

from pyinclude.usefulfuncs import statusprint
from .intervals import overlaplenPair, getOverlapInfo
from .twopathcover import getFullPath, choose2PathCover
from .utilities import getAveOccs

from . import EDGE, M1, M2, NODE, NODE0, Interval, IntervalTup


#Constants:
#CHUNK_SIZE = 250000
CHUNK_SIZE = 100000



class IndexInfo:
  START = 0
  END = 1

  def __init__(self, i: int, start: bool, node: NODE, length: int):
    self.i = i
    self.type = self.START if start else self.END
    self.node = node
    self.length = length  #: The length of the interval associate to this index

  @property
  def start(self):
    return self.type == self.START

  @property
  def end(self):
    return self.type == self.END

  def __str__(self):
    if self.start:
      return f'{self.i} --'
    else:
      return f'-- {self.i} ({self.length})'


class BiBlockGraph(nx.Graph):
  """
  This is a bipartite nx.Graph used for storing overlaps in blocks between
  two methods.  Extra attributes are stored pertaining to the blocks it was
  built from.

  Attributes
  ----------
  m1 : str
      method 1
  m2 : str
      method 2
  block2gid2ints1 : Dict[str, Dict[str, List[Interval]]]
      map block to genome to intervals (there can be multiple in a genome)
  block2gid2ints2 : Dict[str, Dict[str, List[Interval]]]
      map block to genome to intervals (there can be multiple in a genome)
  nodes1 : Set[NODE]
      the nodes corresponding to blocks from method 1
  nodes2 : Set[NODE]
      the nodes corresponding to blocks from method 2
  """

  def buildGraph(self, m1:str,
                 block2gid2ints1: Dict[str, Dict[str, List[Interval]]],
                 m2:str,
                 block2gid2ints2: Dict[str, Dict[str, List[Interval]]],
                 threads=1, cutoff=1) -> None:
    """
    Build the bipartite graph between blocks of `m1` and blocks of `m2` where the
    edges are labeled by the "overlap" between the blocks. The overlap is the
    sum of the number of overlapping nucleotides within each genome between two
    blocks.
  
    Notes
    -----
      Each node is represented by a (method, block, counter) pair.
  
    Parameters
    ----------
    m1 : str
        method 1
    block2gid2ints1 : Dict[str, Dict[str, List[Interval]]]
        map block to genome to intervals (there can be duplicates in a genome)
    m2 : str
        method 2
    block2gid2ints2 : Dict[str, Dict[str, List[Interval]]]
        map block to genome to intervals
    threads : int, optional
        the number of threads to use in parallel, by default 1
    cutoff : int, optional
        only create edges that have at this this much overlap
  
    Returns
    -------
    Tuple[Set[NODE], Set[NODE]]
        the set of nodes for genomes 1 and 2.
    """
    if cutoff > 1:
      raise(NotImplementedError)

    self.m1 = m1
    self.m2 = m2
    self.nodes1: Set[NODE] = set()
    self.nodes2: Set[NODE] = set()
    self.block2gid2ints1 = block2gid2ints1
    self.block2gid2ints2 = block2gid2ints2

    statusprint(f'building block graph...')

      #Organize all intervals by gid:
    gid2ints: Dict[str, List[Interval]] = defaultdict(list)
    for block, gid2ints1 in block2gid2ints1.items():
      for gid, ints in gid2ints1.items():
        for intrvl in ints:
          gid2ints[gid].append(IntervalTup(intrvl.start, intrvl.end, block, M1))

    for block, gid2ints2 in block2gid2ints2.items():
      for gid, ints in gid2ints2.items():
        for intrvl in ints:
          gid2ints[gid].append(IntervalTup(intrvl.start, intrvl.end, block, M2))

      #Add edges for overlaps:
    for gid, ints in gid2ints.items():
      ints.sort(key=lambda x: x.start)      #sort interval list by start index.

      i = 0
      while i < len(ints):
        j = i + 1
        while j < len(ints) and ints[j].start < ints[i].end:
          if ints[i].method != ints[j].method:
            node1 = (ints[i].method, ints[i].block, 0)

            node2 = (ints[j].method, ints[j].block, 0)
            if ints[i].method == M2:
              node1, node2 = node2, node1

            self.nodes1.add(node1)
            self.nodes2.add(node2)

            overlap = min(ints[i].end, ints[j].end) - ints[j].start
            if node1 in self and node2 in self[node1]:
              self[node1][node2]['weight'] += overlap
              self[node1][node2]['gids'].add(gid)
            else:
              #self.add_edge(node1, node2, weight=overlap)
              self.add_edge(node1, node2, weight=overlap, gids=set([gid]))

          j += 1
        i += 1


  def buildGraphSlow(self, m1:str,
                     block2gid2ints1: Dict[str, Dict[str, List[Interval]]],
                     m2:str,
                     block2gid2ints2: Dict[str, Dict[str, List[Interval]]],
                     threads=1, cutoff=1) -> None:
    """
    Build the bipartite graph between blocks of `m1` and blocks of `m2` where the
    edges are labeled by the "overlap" between the blocks. The overlap is the
    sum of the number of overlapping nucleotides within each genome between two
    blocks.
  
    Notes
    -----
      Each node is represented by a (method, block) pair.
  
    Parameters
    ----------
    m1 : str
        method 1
    block2gid2ints1 : Dict[str, Dict[str, List[Interval]]]
        map block to genome to intervals (there can be duplicates in a genome)
    m2 : str
        method 2
    block2gid2ints2 : Dict[str, Dict[str, List[Interval]]]
        map block to genome to intervals
    threads : int, optional
        the number of threads to use in parallel, by default 1
    cutoff : int, optional
        only create edges that have at this this much overlap
  
    Returns
    -------
    Tuple[Set[NODE], Set[NODE]]
        the set of nodes for genomes 1 and 2.
    """
    self.m1 = m1
    self.m2 = m2
    self.nodes1 = set()
    self.nodes2 = set()
    self.block2gid2ints1 = block2gid2ints1
    self.block2gid2ints2 = block2gid2ints2

    statusprint(f'building block graph ({threads} threads)...')
    if threads > 1:
      gl.block2gid2ints1 = block2gid2ints1
      gl.block2gid2ints2 = block2gid2ints2
      chunksize = max(int((len(block2gid2ints1)*len(block2gid2ints2)) / (3*threads)),
                      CHUNK_SIZE)
      with Pool(processes=threads) as pool:
        for b1, b2, overlap, gids in pool.imap_unordered(overlapInfoWrapperG,
                                                         product(block2gid2ints1,
                                                                 block2gid2ints2),
                                                         chunksize=chunksize):
          node1 = (M1, b1, 0)  #(method, block, intervalnum)
          self.nodes1.add(node1)
          node2 = (M2, b2, 0)
          self.nodes2.add(node2)
    
          if overlap >= cutoff:
            self.add_edge(node1, node2, weight=overlap, gids=gids)
  
    else:
      for b1 in block2gid2ints1:
        node1 = (M1, b1, 0)
        self.nodes1.add(node1)
  
        for b2 in block2gid2ints2:
          overlap, gids = getOverlapInfo(block2gid2ints1[b1], block2gid2ints2[b2])
    
          node2 = (M2, b2, 0)
          self.nodes2.add(node2)
          if overlap >= cutoff:
            self.add_edge(node1, node2, weight=overlap, gids=gids)


  def writeGraph(self, fpath: Path):
    """
    Write the given graph and the nodes (from each side of the bipartite graph)
    to the given filename.
    """
    #fpath = Path(filename)
    if not fpath.parent.is_dir():
     fpath.parent.mkdir(parents=True)

    nx.write_gpickle(self, fpath)


  def getSegFrequencies(self, eset: Iterable[EDGE]=(),
                        includezeros=False) -> Tuple[List[float], List[float]]:
    """
    Return a list of the expected number of segments per genome for each
    vertex (block), on each side of the graph.

    Parameters
    ----------
    eset : Iterable[EDGE], optional
        Return lists (of length 2*len(eset)) of segment frequency averages for
        the given set of edges.
    includezeros : bool
        If True, then include genomes that have zero genomes in the average

    Returns
    -------
    Tuple[List[int], List[int]]
        List of expected frequences for block set 1 and then block set 2
    """
    numgenomes = 0
    if includezeros:
      numgenomes = len(set(gid for gid2ints in self.block2gid2ints1.values()
                           for gid in gid2ints.keys()))

    if eset:
      blocks1 = []
      blocks2 = []
      for node in set(chain(*eset)):    #Visit each node only once.
        if node[0] == M1:
          blocks1.append(node[1])
        else:
          blocks2.append(node[1])
    else:
      blocks1 = [n[1] for n in self.nodes1]
      blocks2 = [n[1] for n in self.nodes2]

    return ([getAveOccs(self.block2gid2ints1[b], numgenomes) for b in blocks1],
            [getAveOccs(self.block2gid2ints2[b], numgenomes) for b in blocks2])
    #return ([getAveOccs(self.block2gid2ints1[b]) for b in blocks1],
    #        [getAveOccs(self.block2gid2ints2[b]) for b in blocks2])
  

  def getDupFrequencies(self, eset: Iterable[EDGE]=()) -> Tuple[List[float],
                                                                List[float]]:
    """
    Return a list of the expected number of duplicate segements per genome for
    each vertex (block), on each side of the graph.
    (This differs from getSegFrequency in that it excludes genomes with no
    segments in the average)

    Parameters
    ----------
    eset : Iterable[EDGE], optional
        Return lists (of length 2*len(eset)) of segment frequency averages for
        the given set of edges.

    Returns
    -------
    Tuple[List[int], List[int]]
        List of expected frequences for block set 1 and then block set 2
    """
    return self.getSegFrequencies(eset)


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


class SingleBlockGraph(nx.Graph):
  """
  This is a nx.Graph used for finding blocks that overlap with other from a
  single method.

  Attributes
  ----------
  m1 : str
      method 1
  block2gid2ints1 : Dict[str, Dict[str, List[Interval]]]
      map block to genome to intervals (there can be multiple in a genome)
  """

  def buildGraph(self, method:str,
                 block2gid2ints: Dict[str, Dict[str, List[Interval]]],
                 cutoff=1) -> None:
    """
    Build the graph from the blocks of `m1` where edges are labeled
    edges are labeled by the "overlap" between the blocks. The overlap is the
    sum of the number of overlapping nucleotides within each genome between two
    blocks.
  
    Notes
    -----
      Each node is represented by a block key from `block2gid2ints`.
  
    Parameters
    ----------
    m1 : str
        method 1
    block2gid2ints1 : Dict[str, Dict[str, List[Interval]]]
        map block to genome to intervals (there can be duplicates in a genome)
    threads : int, optional
        the number of threads to use in parallel, by default 1
    cutoff : int, optional
        only create edges that have at this this much overlap
    """
    self.method = method
    self.block2gid2ints = block2gid2ints

    statusprint(f'building block graph...')

      #Organize all intervals by gid:
    gid2ints: Dict[str, List[Interval]] = defaultdict(list)
    for block, gid2ints1 in block2gid2ints.items():
      for gid, ints in gid2ints1.items():
        for interval in ints:
          gid2ints[gid].append(IntervalTup(interval.start, interval.end, block))

      #Add edges for overlaps:
    for gid, ints in gid2ints.items():
      ints.sort(key=lambda x: x.start)      #sort interval list by start index.

      i = 0
      while i < len(ints):
        j = i + 1
        while j < len(ints) and ints[j].start < ints[i].end:
          node1 = ints[i].block
          node2 = ints[j].block

          overlap = min(ints[i].end, ints[j].end) - ints[j].start
          if node1 in self and node2 in self[node1]:
            self[node1][node2]['weight'] += overlap
          else:
            self.add_edge(node1, node2, weight=overlap)

          j += 1
        i += 1


  def writeGraph(self, fpath: Path):
    """
    Write the given graph to the given filename.
    """
    #fpath = Path(filename)
    if not fpath.parent.is_dir():
     fpath.parent.mkdir(parents=True)

    nx.write_gpickle(self, fpath)


class MDDSDP:
  """
  The Minimum Deletion into Dijoint Stars dynamic program.
  """
  DSTAR, DPLUS, DMINUS = 0, 1, 2              #where the solution came from

  def __init__(self, tree: nx.Graph):
    """
    Compute the Minimum Deletion into Disjoint Stars for the given tree.
    """
    if len(tree) <= 2:
      self.remset: Optional[List[EDGE]] = []
      self.weight = 0
      return

      #Compute the Minimum Deletion in Disjoint Stars on the tree:
      # (see Section 3.2 of paper for description)
    Dstar: Dict[NODE, int] = {}
    Dplus: Dict[NODE, int] = {}
    Dminus: Dict[NODE, int] = {}
    vstack: List[NODE] = []                     #node stack
    nstack: List[Iterator[NODE]] = []           #neighbor stack

          #Traceback variables:
    DstarTB: Dict[NODE, Tuple[bool, int]] = {}  #map child to (removed?, choice)
    DminusTB: Dict[NODE, Tuple[bool, int]] = {}
    DplusTB: Dict[NODE, Tuple[bool, int]] = {}  #removed? always True in this case
    DminusSAVED: Dict[NODE, NODE] = defaultdict(lambda: NODE0)
                                                #edge not removed (parent key)

          #Visit tree in post-order traversal filling in the D dictionaries.
          #Set traceback variables for each child, indicating if the edge to
          #the parent was removed, and indicating which subsolution was taken
          #for the minimization.
    v = next(iter(tree))        #choose arbitary root

    root = v
    vstack.append(v)
    nstack.append(tree.neighbors(v))
    while vstack:
      v = vstack[-1]
      if v != root and tree.degree[v] == 1:           #at a leaf
        Dstar[v] = 0
        Dplus[v] = 0
        Dminus[v] = 0
        vstack.pop()
        nstack.pop()
        continue

      try:                      #try to get the next child
        child = next(nstack[-1])
        if len(vstack) > 1 and child == vstack[-2]:   #skip the parent
          child = next(nstack[-1])

      except StopIteration:     #no children left to descend to in iterator
        Ds = 0
        Dp = 0
        Dm = 0
        for child in sorted(tree.neighbors(v)):       #Accumulate subsolutions:
          if len(vstack) < 2 or child != vstack[-2]:  #not parent

            w = tree[v][child]['weight']

              #Dstar:
            removalcost = w + min(Dstar[child], Dminus[child])
            Ds += min(Dplus[child], removalcost)

            if Dplus[child] < removalcost:
              DstarTB[child] = (False, self.DPLUS)
            else:
              choice = self.DMINUS
              if Dstar[child] < Dminus[child]:
                choice = self.DSTAR
              DstarTB[child] = (True, choice)

              #Dminus:
            removalcost = Dm + w + min(Dstar[child], Dminus[child])
            Dm = min(removalcost, Dp + Dstar[child])  #min(Dstar[child], Dplus[child])
            if Dp + Dstar[child] >= removalcost:
              choice = self.DMINUS
              if Dstar[child] < Dminus[child]:
                choice = self.DSTAR
              DminusTB[child] = (True, choice)
            else:
              DminusTB[child] = (False, self.DSTAR)
              DminusSAVED[v] = child

              #Dplus (must be updated after Dm):
            Dp += w + min(Dstar[child], Dminus[child])
            choice = self.DSTAR if Dstar[child] <= Dminus[child] else self.DMINUS
            DplusTB[child] = (True, choice)


        Dstar[v] = Ds
        Dplus[v] = Dp
        Dminus[v] = Dm
        vstack.pop()
        nstack.pop()

      else:                     #descend to child
        vstack.append(child)
        nstack.append(tree.neighbors(child))

    self.weight = min(Dstar[root], Dplus[root], Dminus[root])

    self.remset = None
    self.root = root
    self.tree = tree
    self.Dstar = Dstar
    self.Dplus = Dplus
    self.Dminus = Dminus
    self.DstarTB = DstarTB     #map child to (removed?, choice) tuple
    self.DplusTB = DplusTB
    self.DminusTB = DminusTB
    self.DminusSAVED = DminusSAVED


  def getWeight(self) -> int:
    return self.weight
  

  def getRemovalSet(self) -> List[EDGE]:
    """
    Traceback throught the DP to get the removal set.
    """
    if self.remset == None:
      self.remset = self._traceBackDP(self.root)

    return self.remset


  def _traceBackDP(self, root: NODE):
    """
    Trace back through the DP using `DstarTB`, `DplusTB`, `DminusTB`.
    """
    removed: List[EDGE] = []

    best = min(self.Dstar[root], self.Dplus[root], self.Dminus[root])

    choice = self.DMINUS
    if self.Dstar[root] == best:
      choice = self.DSTAR
    elif self.Dplus[root] == best:
      assert False, 'This should never be better than the others for the root.'
      choice = self.DPLUS

    self._traceBackDP_REC(root, NODE0, choice, removed)
    return removed


  def _traceBackDP_REC(self, v: NODE,
                       parent: NODE,
                       choice: int,
                       remlist: List[EDGE]):
    """
    Trace back through the DP using `DstarTB`, `DplusTB`, `DminusTB`.
    The `self.DstarTB` dictionary maps child to (removed?, choice) tuple, where
    removed? is True iff the ancestral edge (`v`, `child`) was removed in the
    solution that scores the subtree rooted at `v` (and assumes that `v` is the
    center of a star, when using `self.DstarTB[child]`)
    """
        #Visit children in order guarantee for DMINUS, since it is only the
        #last of the non-removed that is actually removed.
    savedseen = False
    for child in sorted(self.tree.neighbors(v)):
      if child != parent:
        if choice == self.DSTAR:
          removed, newchoice = self.DstarTB[child]

        elif(choice == self.DMINUS and
             (self.DminusSAVED[v] == child or savedseen)):
          removed, newchoice = self.DminusTB[child]
          savedseen = True

        else:                                      #DPLUS, or DMINUS and we've not
          removed, newchoice = self.DplusTB[child] #seen the saved child edge yet
        
        if removed:
          remlist.append((v, child))
    
        self._traceBackDP_REC(child, v, newchoice, remlist)
    

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Functions:
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


def readSingleGraph(filename: str|Path) -> SingleBlockGraph:
  """
  Read the graph from the given filename.
  """
  G = nx.read_gpickle(filename)
  if not isinstance(G, SingleBlockGraph):
    raise(Exception(f'Expected SingleBlockGraph but opened a {type(G)}.'))

  return G


def readGraph(filename: str|Path) -> BiBlockGraph:
  """
  Read the graph from the given filename.
  """
  G = nx.read_gpickle(filename)
  if not isinstance(G, BiBlockGraph):
    raise(Exception(f'Expected BiBlockGraph but opened a {type(G)}.'))

  return G


def getGraph(m1:str, block2gid2ints1: Dict[str, Dict[str, List[Interval]]],
             m2:str, block2gid2ints2: Dict[str, Dict[str, List[Interval]]],
             threads=1, filename='', cutoff=1
             ) -> BiBlockGraph:
  """
  Either load a graph from `filename`, if it exists, or build a graph. Save
  it at `filename` if it is specified.  See `buildGraph()` for more details.
  """
  if filename and Path(filename).exists():
    G = readGraph(filename)
  else:
    G = BiBlockGraph()
    G.buildGraph(m1, block2gid2ints1, m2, block2gid2ints2, threads, cutoff)
    if filename:
      G.writeGraph(filename)
  
  return G


def overlapInfoWrapperG(blocks: Tuple[str, str]
                        ) -> Tuple[str, str, int, List[str]]:
  """
  Call `getOverlapInfo` on the given pair of lists.

  Notes
  -----
    Uses the globals `gl.block2gid2ints1` and `gl.block2gid2ints2`
    so that the memory doesn't need to be copied for each process of
    starmap_async().

  Returns
  -------
  Tuple[str, str, int, List[str]]
      (b1, b2, overlap, gidlist) where b1 is the first method, b2 is the second
      method, overlap is the overlap between them, and gidlist holds the genomes
      where there is an overlap.
  """
  overlap, genomes = getOverlapInfo(gl.block2gid2ints1[blocks[0]],
                                    gl.block2gid2ints2[blocks[1]])
  return (blocks[0], blocks[1], overlap, genomes)


def showGraph(G: nx.Graph, filename='', gidthreshold=0,
              highlight: Collection[EDGE]=set(), side1: Set[NODE]=set(),
              bipartitelayout=False, nodeheat: Dict[NODE, float]={},
              labelnodes=False, node2label={}):
  """
  Either show or draw the given graph (depending on filename).

  Parameters
  ----------
  G : nx.Graph
      the graph to show
  filename : str, optional
      if specified, write the graph to this file instead of showing it,
      by default ''
  highlight : Collection[EDGE], optional
      a set of edges to highlight, by default set()
  gidthreshold : int, optional
      show the gids where the blocks intersect if there are less than this
      many gids to show, by default 0
  side1 : Collection[NODE], optional
      highlight these nodes as being different from the others
  bipartitelayout : bool, optional
      use the bipartite graph layout
  nodeheat : Dict[NODE, float], optional
      color the nodes according to this value (between 0 and 1)
  labelnodes : bool, optional
      draw node labels
  node2label : Dict, optional
      display label mapped to by node
  """
  #G.graph['edge'] = {'splines': 'true'}  #must convert to Agraph
  if bipartitelayout and side1:
    pos = nx.bipartite_layout(G, side1)
  else:
    #pos = nx.spring_layout(G)
    pos = nx.nx_pydot.graphviz_layout(G, prog='dot')

    #Color the edges:
  edges = list(G.edges())
  ecolors = ['k'] * len(edges)
  ethickness = [1.0] * len(edges)
  for i, (u, v) in enumerate(edges):            #type: ignore
    if (u, v) in highlight or (v, u) in highlight:
      ecolors[i] = 'r'
      ethickness[i] = 0.3

  height = math.sqrt(len(G))
  plt.figure(1, figsize=(height*1.5, height))
  if nodeheat:
    side2 = G.nodes - side1
    assert not side1 & side2
    nodesize = 20
    heats1 = [nodeheat[n] for n in side1]
    nx.draw_networkx_nodes(G, pos, nodelist=side1,
                           node_size=nodesize, node_color=heats1,
                           cmap=plt.get_cmap('jet'))

    heats2 = [nodeheat[n] for n in side2]
    nx.draw(G, pos, nodelist=side2, node_shape='v', with_labels=False,
            node_size=nodesize, node_color=heats2,
            cmap=plt.get_cmap('jet'), edgelist=edges, edge_color=ecolors,
            width=ethickness)

    if labelnodes:          #Node labels.
      if node2label:
        nlabels = node2label
      else:
        nlabels = {node: str(node[1]) for node in G}

      nx.draw_networkx_labels(G, labels=nlabels, font_size=7, with_labels=True,
                              pos={n: (p[0], p[1]-5) for n, p in pos.items()})  #type: ignore
  else:
    ncolors = ['black' if node in side1 else 'lightgray' for node in G]

    nx.draw(G, pos, with_labels=False, node_size=10, edgelist=edges,
            edge_color=ecolors, width=ethickness, node_color=ncolors)

    #Label the edges:
  if gidthreshold:
    labels = {}
    for u, v, data in G.edges(data=True):       #type: ignore
      labelstr = f'{data["weight"]}'            #type: ignore
      if 'gids' in data:                        #type: ignore
        labelstr += f', {len(data["gids"])}'    #type: ignore
        if len(data['gids']) <= gidthreshold:   #type: ignore
          labelstr += f': {data["gids"]}'       #type: ignore

      labels[(u, v)] = labelstr
  else:
    #labels = nx.get_edge_attributes(G, 'weight')
    labels = {}
    for u, v, data in G.edges(data=True):           #type: ignore
      labels[(u, v)] = f'{data["weight"]}'          #type: ignore
      if 'gids' in data:                            #type: ignore
        labels[(u, v)] += f', {len(data["gids"])}'  #type: ignore

  nx.draw_networkx_edge_labels(G, pos, edge_labels=labels,
                               label_pos=0.3, font_size=4.0)

  if filename:
    #if Path(filename).exists():
    #  #exit(f'"{filename}" already exists (move it or change COMP_PREFIX).')
    #  #raise(Exception(f'file "{filename}" already exists!'))
    #  eprint(f'Skipping "{filename}" since it already exists.')
    plt.savefig(filename)
  else:
    plt.show()

  plt.clf()


def showEmptyGraph(numnodes, filename=''):
  """
  Draw an empty graph with a descriptive node label.
  """
  G = nx.Graph()
  G.add_node(f'Graph has too many vertices! ({numnodes} > TOO_BIG_TO_SHOW)')
  nx.draw(G, node_size=1, with_labels=True)
  
  if filename:
    if Path(filename).exists():
      #raise(Exception(f'file "{filename}" already exists!'))
      exit(f'"{filename}" already exists (move it or change COMP_PREFIX).')
    plt.savefig(filename)
  else:
    plt.show()

  plt.clf()
  

def scorePath(path: nx.Graph, edges: List[EDGE]):
  return sum(path.edges[edge]['weight'] for edge in edges)


def getEdgeNeighborhood(G: nx.Graph, edge: EDGE,
                        block2gid2ints1: Dict[str, Dict[str, List[Interval]]],
                        block2gid2ints2: Dict[str, Dict[str, List[Interval]]]
                        ) -> str:
  """
  Return overlap information about all the blocks touching this edges.

  Parameters
  ----------
  G : nx.Graph
      the component the edge is in
  edge : EDGE
      the edge
  block2gid2ints1 : Dict[str, Dict[str, List[Interval]]]
      map block to genome to intervals in the block
  block2gid2ints2 : Dict[str, Dict[str, List[Interval]]]
      map block to genome to intervals in the block
  """
  u, v = edge
  rs = f'|||||||||||||| {u[0]} {u[1]} -- {v[0]} {v[1]} ||||||||||||||||||||\n\n'
  gid2indices = mapGenome2Indices(u, [v], block2gid2ints1, block2gid2ints2)
  rs += getNeighborhoodTable(u, [v], gid2indices)

  rs += f'||||||||||||||||| {u[0]} {u[1]} |||||||||||||||||||||||\n\n'
  rs += getNodeNeighborhood(G, u, v, block2gid2ints1, block2gid2ints2)

  rs += f'||||||||||||||||| {v[0]} {v[1]} |||||||||||||||||||||||\n\n'
  rs += getNodeNeighborhood(G, v, u, block2gid2ints1, block2gid2ints2)

  return rs


def getNodeNeighborhood(G: nx.Graph, node: NODE, other: NODE,
                        block2gid2ints1: Dict[str, Dict[str, List[Interval]]],
                        block2gid2ints2: Dict[str, Dict[str, List[Interval]]]):
  """
  Return overlap information about all the blocks touching `node` that are not
  `other`.
  """
  neighbors = sorted([v for v in G.neighbors(node) if v != other])

  gid2indices = mapGenome2Indices(node, neighbors, block2gid2ints1,
                                  block2gid2ints2)

  return getNeighborhoodTable(node, neighbors, gid2indices)


def mapGenome2Indices(node, neighbors: List[NODE],
                      block2gid2ints1: Dict[str, Dict[str, List[Interval]]],
                      block2gid2ints2: Dict[str, Dict[str, List[Interval]]]
                      ) -> Dict[str, Set[IndexInfo]]:
  """
  Return a mapping from gid to IndexInfo, which represets the start and end
  index of an Interval.
  """
  gid2indices: Dict[str, Set[IndexInfo]] = defaultdict(set)
  for v in neighbors:
    m1, b1, _ = node
    m2, b2, _ = v             #type: ignore
    if m1 == M1:
      g2i1, g2i2 = block2gid2ints1[b1], block2gid2ints2[b2]
    else:
      assert m1 == M2
      g2i1, g2i2 = block2gid2ints2[b1], block2gid2ints1[b2]

    gids = sorted(set(g2i1) & set(g2i2))

    for gid in gids:
      for i in g2i1[gid]:
        gid2indices[gid].add(IndexInfo(i.start, True, node, len(i)))
        gid2indices[gid].add(IndexInfo(i.end, False, node, len(i)))

      for i in g2i2[gid]:
        gid2indices[gid].add(IndexInfo(i.start, True, v, len(i))) #type: ignore
        gid2indices[gid].add(IndexInfo(i.end, False, v, len(i)))  #type: ignore

  return gid2indices


def getNeighborhoodTable(node: NODE, neighbors: List[NODE],
                           gid2indices: Dict[str, Set[IndexInfo]]) -> str:
  retstr = ''
  allnodes = [node] + neighbors
  for gid, iset in gid2indices.items():
    ilist = sorted(iset, key=lambda x: x.i)
    block2row: Dict[NODE, List[str]] = defaultdict(list)

    prev = -1
    block2in = {n: False for n in allnodes}
    for index in ilist:
      if prev == index.i:
        block2row[index.node][-1] = str(index)

      else:
        for v in allnodes:
          if v == index.node:
            block2row[v].append(str(index))
          else:
            if block2in[v]:
              block2row[v].append('------------')
            else:
              block2row[v].append('')

      block2in[index.node] = True if index.start else False

      prev = index.i

    t = PrettyTable(header=False, vrules=NONE, hrules=NONE,
                    padding_width=0)
                    #left_padding_width=0,
                    #right_padding_width=0)
    #t.set_style(PLAIN_COLUMNS)
    for n in allnodes:
      #t.add_row([f'{n[0]} {n[1]}'] + block2row[n])
      if n == node:
      #  t.add_row([''] + block2row[n])
        t.add_row([f'ref: {n[0]} {n[1]}'] + block2row[n])
      else:
        t.add_row([f'{n[0]} {n[1]}'] + block2row[n])

    retstr += f'{gid}:\n'
    retstr += makeFancy(t.get_string()) + '\n'

  return retstr


def makeFancy(table: str) -> str:
  """
  Return a version of the table that looks better.

  Parameters
  ----------
  table : str
      the table in PrettyTable string format.

  Returns
  -------
  str
      the fancy version of `table`
  """
  newstr = ''
  openre = re.compile(r'\d --')
  closere = re.compile(r'-- \d')
  for line in table.splitlines():
    ll = list(line)

    i = 8
    open = False
    while i < len(line):
      if openre.match(line[i-4:i]):
        open = True
      elif closere.match(line[i-2:i+2]):
        open = False

      if open:
        ll[i] = '-'
      
      i += 1

    newstr += ''.join(ll) + '\n'

  return newstr

  
def isStarPacking(G: nx.Graph) -> bool:
  """
  Return True if all connected components of the given graph are stars.
  """
  for cc in nx.connected_components(G):
    if len(cc) > 1 and getDegreesOfSubgraph(G, cc)[-2] != 1:
      return False

  return True


def isMatching(G: nx.Graph) -> bool:
  """
  Return True if all connected components of the given graph are single edges.
  """
  for cc in nx.connected_components(G):
    if len(cc) > 2:
      return False

  return True


def getDegreesOfSubgraph(G: nx.Graph, nodes: List[NODE]) -> List[int]:
  return sorted(d for _, d in nx.induced_subgraph(G, nodes).degree)


def getSplitGraph(G: nx.Graph,
                  block2gid2ints1: Dict[str, Dict[str, List[Interval]]],
                  block2gid2ints2: Dict[str, Dict[str, List[Interval]]]
                  ) -> Tuple[nx.Graph, Set[NODE]]:
    """
    If `G` is not a star decomposition, get a version of the graph where some
    nodes have been split by the intervals in the blocks so that there is a star
    decomposition.

    Notes
    -----
    Use the overlap of the blocks between the two methods to decide which copy
    of duplicated segments are orthologous.  Intervals that don't overlap with
    an interval from another block are grouped into the first node that is
    created.

    An alternative, and more comprehensive approach would be to build a graph
    representing the overlapping of the intervals (rather than blocks).  Then
    the connected components of this graph that have only a single interval
    from each block would represent the orthologs, and blocks could be split
    accordingly.
    """
    if isStarPacking(G):
      return G, set()

    newG = nx.Graph(G)
    newnodes1 = set()
    for n in list(newG):
      if n[0] == M1:
        ngid2ints = block2gid2ints1[n[1]]
        block2gid2ints = block2gid2ints2
      else:
        ngid2ints = block2gid2ints2[n[1]]
        block2gid2ints = block2gid2ints1

      addedone = False
      if hasMultipleIntervals(ngid2ints):
          #Assign each interval to the blocks that it overlaps with and then
          # check that each interval is mapped uniquely to a block:
          #First, get the neighbors that overlap with intervals in n.
        int2blocks = assignIntsToNodes(newG, n, ngid2ints, block2gid2ints)
        #seperable = True
        #for i, blocklist in int2blocks.items():
        #  if len(blocklist) > 1:
        #    seperable = False

          #Split the node (block) based on int2blocks:
        newb2gid2ints: Dict[NODE, Dict[str, List[Interval]]] = \
          defaultdict(lambda: defaultdict(list))

        for gid, intlist in ngid2ints.items():
          for i in intlist:
            if i in int2blocks:       #if i overlaps with a neighbor node
              newb2gid2ints[int2blocks[i][0]][gid].append(i)
            else:
              newb2gid2ints[NODE0][gid].append(i)

        for j, v in enumerate(newb2gid2ints):
          if v != NODE0:              # no node for non-overlapping intervals
            assert(v[0] != n[0])
            newnode = (n[0], n[1], j+1)
            newG.add_node(newnode)
            if n[0] == M1:
              newnodes1.add(newnode)

            overlap, gids = getOverlapInfo(ngid2ints, newb2gid2ints[v])
            newG.add_edge(newnode, v, weight=overlap, gids=gids)
            addedone = True

      if addedone:
        newG.remove_node(n)
      elif n[0] == M1:
        newnodes1.add(n)

    return newG, newnodes1


def assignIntsToNodes(G: nx.Graph, n: NODE,
                      ngid2ints: Dict[str, List[Interval]],
                      block2gid2ints) -> Dict[Interval, List[NODE]]:
  """
  Assign intervals in `n` to the neighboring nodes that overlap with them.
  """
  int2block: Dict[Interval, List[NODE]] = defaultdict(list)
  for u in G.neighbors(n):
    for gid in ngid2ints:
      for i in ngid2ints[gid]:
        for j in block2gid2ints[u[1]][gid]:
          if overlaplenPair(i, j):
            int2block[i].append(u)

  return int2block


def getGid2Ints(n: NODE,
                block2gid2ints1: Dict[str, Dict[str, List[Interval]]],
                block2gid2ints2: Dict[str, Dict[str, List[Interval]]]
                ) -> Dict[str, List[Interval]]:
  if n[0] == M1:
    return block2gid2ints1[n[1]]

  return block2gid2ints2[n[1]]


def hasMultipleIntervals(gid2ints: Dict[str, List[Interval]]):
  """
  Does one of the interval lists have more than one interval.
  """
  for intlist in gid2ints.values():
    if len(intlist) > 1:
      return True

  return False


def getMinStarCutSet(G: nx.Graph, verbose=False) -> Tuple[List[EDGE], int]:
  """
  A valid assignment of blocks between two methods does not have a connected
  component that has more than one vertex in both side of the bipartite
  graph (i.e. every valid connected component is a star).
  The "star cut set" is a set of edges that must be left out in order to
  make a valid assignment (a star packing). A minimum cut set is one of
  minimum weight (edges are labeled by the number of nucleotides overlapping
  by the blocks from the two methods).
  """
  forest = tree.maximum_spanning_tree(G)

    #Record the edges not in the forest:
  weight = 0
  removed = []     #Remove these edges from the graph to have only stars
  for u, v, data in G.edges(data=True):     #type: ignore
    if v not in forest[u]:
      if verbose:
        print(f'Not in max forest:\n  {u, v} : {data}')
      removed.append((u, v))
      weight += data['weight']              #type: ignore

  r, w = MDDSonForest(forest)
  weight += w
  removed.extend(r)

    #Verify only stars are left, and add non-spanning-tree edges that can be
    # put back in:
  Gcopy = G.copy()
  Gcopy.remove_edges_from(removed)

    #If there is a lone vertex, then connect it in the best way:
  for cc in list(nx.connected_components(Gcopy)):
    if len(cc) == 1:
      v = next(iter(cc))
      if Gcopy.degree[v]:
        continue            #An edge was added to v in previous iteration.

      bestv = NODE0
      bestw = 0
      for u in G.neighbors(v):
        if canBeStarCenter(Gcopy, u) and G[u][v]['weight'] > bestw:
          bestv = u
          bestw = G[u][v]['weight']

      if bestv != NODE0:
        weight -= bestw
        Gcopy.add_edge(v, bestv, weight=bestw)
        try:
          removed.remove((v, bestv))
        except ValueError:
          removed.remove((bestv, v))

    assert isStar(cc), f'Component not a star: {cc}\n{nx.induced_subgraph(Gcopy, cc).edges()}'

    #Verify that at least one side has a single vertex:
  for cc in nx.connected_components(Gcopy):
    assert isStar(cc), f'Component not a star: {cc}'

  return removed, weight


def isStar(cc: Iterable[NODE]) -> bool:
  counts: Counter = Counter()
  for n in cc:
    counts[n[0]] += 1

  return ((counts[M1] <= 1 or counts[M2] <= 1) and
          (counts[M1] > 0 or counts[M2] > 0))


def canBeStarCenter(G: nx.Graph, v: NODE) -> bool:
  """
  Return True if `v` can be the center of a star where G is a star
  decomposition.
  """
  return not (G.degree[v] == 1 and G.degree[next(G.neighbors(v))] > 1)


def MDDSonForest(forest: nx.Graph, verify=True) -> Tuple[List[EDGE], int]:
  """
  Compute the Minimum Deletion into Disjoint Stars for the given forest.
  Return the cost of the deletion and the deletion set.
  """
  weight = 0
  removal = []
  for tree in nx.connected_components(forest):
    dp = MDDSDP(nx.induced_subgraph(forest, tree))
    weight += dp.getWeight()
    remset = dp.getRemovalSet()
    remsetw = sum(forest.edges[u,v]['weight'] for u, v in remset)

    if verify and dp.getWeight() != remsetw:
      print(f'Weight mismatch: {dp.getWeight()} != {remsetw}\nInstance:')
      for u, v, data in nx.induced_subgraph(forest, tree).edges(data=True): #type: ignore
        print(f'{"_".join(map(str, u))} {"_".join(map(str, v))} '
              f'{data["weight"]}')                                          #type: ignore

      #assert weight == remsetw, f'Weight mismatch: {weight} != {remsetw}'

    removal.extend(remset)
    
  return removal, weight


def getMinStarCutSetOLD(G: nx.Graph, verbose=False) -> Tuple[List[EDGE],  int]:
  """
  A valid assignment of blocks between two methods does not have a connected
  component that has more than one vertex in both side of the bipartite
  graph (i.e. every valid connected component is a star).
  The "star cut set" is a set of edges that must be left out in order to
  make a valid assignment (a star packing). A minimum cut set is one of
  minimum weight (edges are labeled by the number of nucleotides overlapping
  by the blocks from the two methods).
  """
  #forestedges = set(tree.maximum_spanning_edges(G, data=False))
  forest = tree.maximum_spanning_tree(G)

    #Record the edges not in the forest:
  weight = 0
  removed = []     #Remove these edges from the graph to have only stars
  for u, v, data in G.edges(data=True):     #type: ignore
    if v not in forest[u]:
      if verbose:
        print(f'Not in max forest:\n  {u, v} : {data}')
      removed.append((u, v))
      weight += data['weight']              #type: ignore

    #Find all the paths in the graph of degree-2 nodes:
  degree2 = set()
  for v in forest.nodes:
    if forest.degree[v] == 2:
      degree2.add(v)

  pathsG = nx.induced_subgraph(forest, degree2)

    #Figure out which edges within each path we will keep:
  for path in nx.connected_components(pathsG):
    remove = choose2PathCover(forest, path)
    removed.extend(remove)
    remweights = [forest.edges[edge]['weight'] for edge in remove]
    weight += sum(remweights)

    pathweight = scorePath(forest, getFullPath(forest, path))
    if verbose:
      print(f'Removed {sum(remweights)} of {pathweight} nucs:\n  '
            f'{list(zip(remove, remweights))}')

    #Verify only stars are left:
  if verbose:
    Gcopy = G.copy()
    Gcopy.remove_edges_from(removed)
    for cc in nx.connected_components(Gcopy):
      counts: Counter = Counter()
      for n in cc:
        counts[n[0]] += 1

      assert ((counts[M1] <= 1 or counts[M2] <= 1) and
              (counts[M1] > 0 or counts[M2] > 0)), \
             f'Component not a star: {counts}'

  return removed, weight


def readGraphWithAttrs(filename: str) -> BiBlockGraph:
  """
  Read the nx.Graph with the other attributes, returning a BiBlockGraph.
  """
  G = nx.read_gpickle(filename)
  if isinstance(G, BiBlockGraph):
    raise(Exception('Expected nx.Graph instead of BiBlockGraph.'))

  elif not isinstance(G, nx.Graph):
    raise(Exception('Unknown graph type in file.'))

  bg = BiBlockGraph(G)
  bg.m1 = G.graph['m1']
  bg.m2 = G.graph['m2']
  bg.nodes1 = G.graph['nodes1']
  bg.nodes2 = G.graph['nodes2']
  bg.block2gid2ints1 = G.graph['block2gid2ints1']
  bg.block2gid2ints2 = G.graph['block2gid2ints2']
 
  return bg


def compareGraphDirs(dir1: str, dir2: str):
  """
  Compare each graph in dir1 one to the corresponding graph ni dir2.
  """
  for p1 in Path(dir1).iterdir():
    p2 = Path(dir2, p1.name)
    print(p1)
    try:
      compareGraphs(readGraph(str(p1)), readGraph(str(p2)))
    except GraphDifferenceError as e:
      print(e)


def compareGraphs(g1: nx.Graph, g2: nx.Graph):
  """
  Raise and error if there is a difference between the graphs.
  """
  diffmessage = ''
  if len(g1) != len(g2):
    diffmessage = f'Number of nodes differs: {len(g1)} != {len(g2)}\n'

  for u in set(chain(g1, g2)):
    if u not in g1:
      raise(GraphDifferenceError(f'{diffmessage}Missing node in g1: {u}'))
    if u not in g2:
      raise(GraphDifferenceError(f'{diffmessage}Missing node in g2: {u}'))
    
    neigh1 = set(g1.neighbors(u))
    neigh2 = set(g2.neighbors(u))

    if neigh2 - neigh1:
      raise(GraphDifferenceError(f'{diffmessage}Missing edges in g1: {neigh2 - neigh1}'))
    if neigh1 - neigh2:
      raise(GraphDifferenceError(f'{diffmessage}Missing edges in g2: {neigh1 - neigh2}'))

    for v in g1.neighbors(u):
      if g1[u][v]['weight'] != g2[u][v]['weight']:
        raise(GraphDifferenceError(f'{diffmessage}Weight mismatch for edge {(u, v)}: {g1[u][v]["weight"]} != {g2[u][v]["weight"]}'))
      if set(g1[u][v]['gids']) != set(g2[u][v]['gids']):
        raise(GraphDifferenceError(f'{diffmessage}GIDset mismatch for edge {(u, v)}: {set(g1[u][v]["gids"]) ^ set(g2[u][v]["gids"])}'))

class GraphDifferenceError(Exception):
  pass
