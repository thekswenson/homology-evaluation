import csv
import pybedtools
import gzip
import gc
import gffutils

from pathlib import Path, Path
from typing import Deque, Dict, List, Set, Tuple, Iterable
from collections import deque
from collections import defaultdict as dd
#from tempfile import NamedTemporaryFile
import pandas as pd
from Bio.AlignIO import MafIO

from .utilities import CompAnalysis

from . import IntervalTup, Interval


#Constants:
BED_EXTS = ['.bed.gz', '.bed']
MAF_EXTS = ['.maf', '.maf.gz']
GFF_EXTS = ['.gff', '.gff.gz']
BLOCK_EXTS = BED_EXTS + MAF_EXTS + GFF_EXTS

HEADERS = ('Datasets', 'Union', 'Intersection', 'Coverage', 'Coverage',
           'Stars >', 'Stars >', 'Big components', 'Cut ratio',
           'Ignore ratio', 'Ignore threshold', '(split) Stars >',
           '(split) Stars >', '(split) Big components', '(split) Cut ratio')

class CSVInfo:
  """
  Hold information from a single comparison pair of a CSV summary file.

  Notes
  -----
      Should have done this with a pandas.DataFrame.
  """
  def __init__(self, lines: Deque):
    try:
      i = 0
      while i < len(HEADERS):
        line = lines.popleft()
        if HEADERS[i] not in line[0]:
          raise(Exception(f'Unexpected CSV entry: got "{line[0]}" but expected "{HEADERS[i]}"'))

        if i == 0:
          self.m1 = line[1]
          self.m2 = line[2]
        elif i == 1:
          self.union = int(line[1])
        elif i == 2:
          self.intersection = int(line[1])
        elif i == 3:
          self.intercoveragerat1 = float(line[1])
          self.coverage1 = int(line[2])
        elif i == 4:
          self.intercoveragerat2 = float(line[1])
          self.coverage2 = int(line[2])
        elif i == 5:
          self.stars1 = int(line[1])
        elif i == 6:
          self.stars2 = int(line[1])
        elif i == 7:
          self.big = int(line[1])
        elif i == 8:
          self.cutratio = float(line[1])
        elif i == 9:
          self.ignoreratio = float(line[1])
        elif i == 10:
          self.ignorethreshold = float(line[1])
        elif i == 11:
          self.sstars1 = int(line[1])
        elif i == 12:
          self.sstars2 = int(line[1])
        elif i == 13:
          self.sbig = int(line[1])
        elif i == 14:
          self.scutratio = float(line[1])
        
        i += 1

    except IndexError as e:
      raise(Exception(f'CSV file ended unexpectedly.'))


def ddlist():
  """
  Use this in place of lambda when the object will be pickled.
  """
  return dd(list)


def writeSummaryCSV(path: Path, method1, method2, intersection, union,
                    coverage1, coverage2, companalysis: CompAnalysis):
  """
  Write a CSV file with rows describing the analysis.
  """
  assert companalysis.initialized()

  with open(path, 'w', newline='') as csvfile:
    w = csv.writer(csvfile, quoting=csv.QUOTE_NONNUMERIC)
    w.writerow(['Datasets', method1, method2])
    w.writerow(['Union', union])
    w.writerow(['Intersection', intersection])
    w.writerow([f'Coverage {method1}', intersection/coverage1, coverage1])
    w.writerow([f'Coverage {method2}', intersection/coverage2, coverage2])
    w.writerow([f'Stars > {method1}', companalysis.stars1])
    w.writerow([f'Stars > {method2}', companalysis.stars2])
    w.writerow([f'Big components', companalysis.big])
    w.writerow([f'Cut ratio', companalysis.cutratio])
    w.writerow([f'Ignore ratio', companalysis.ignoreratio])
    w.writerow([f'Ignore threshold', companalysis.threshold])
    w.writerow([f'(split) Stars > {method1}', companalysis.splitstars1()])
    w.writerow([f'(split) Stars > {method2}', companalysis.splitstars2()])
    w.writerow([f'(split) Big components', companalysis.splitbig()])
    w.writerow([f'(split) Cut ratio', companalysis.splitcutratio])


def writeCoordsDF(path: Path,
                  gid2coords1: Dict[str, List],
                  gid2coords2: Dict[str, List],
                  methodname1: str,
                  methodname2: str):
  """
  Write a DataFrame with rows describing the analysis.
  """
  rows = []
  comparison = f'{methodname1}_VS_{methodname2}'
  for gid, l in gid2coords1.items():
    for i in sorted(l, key=lambda i: i.start):
      rows.append([methodname1, gid, i.start, i.end, comparison])

  for gid, l in gid2coords2.items():
    for i in sorted(l, key=lambda i: i.start):
      rows.append([methodname2, gid, i.start, i.end, comparison])

  df = pd.DataFrame(rows,
                    columns=['Method', 'Genome', 'Start', 'End', 'Comparison'])
  df.to_csv(path, index=False)


def readCoordsDF(infile: str) -> pd.DataFrame:
  """
  Read a DataFrame containing lines with
  [method, genome, start, end, comparison].
  """
  return pd.read_csv(infile)


def writeCoordsCSV(path: Path,
                   gid2coords1: Dict[str, List],
                   gid2coords2: Dict[str, List],
                   methodname1: str,
                   methodname2: str):
  """
  Write a CSV file with rows describing the analysis.
  """
  with open(path, 'w', newline='') as csvfile:
    w = csv.writer(csvfile, quoting=csv.QUOTE_NONNUMERIC)

    for gid, l in gid2coords1.items():
      for i in sorted(l, key=lambda i: i.start):
        w.writerow([methodname1, gid, i.start, i.end])

    for gid, l in gid2coords2.items():
      for i in sorted(l, key=lambda i: i.start):
        w.writerow([methodname2, gid, i.start, i.end])


def readCoordsCSV(infile: str) -> Dict[str, Dict[str, List[IntervalTup]]]:
  """
  Read a CSV file containing lines with [method, genome, start, end] and return
  a dictionary mapping genome to block to list of intervals.
  """
  m2gid2ints: Dict[str, Dict[str, List[IntervalTup]]] = dd(lambda: dd(list))
  with open(infile, newline='') as f:
    for line in csv.reader(f):
      m2gid2ints[line[0]][line[1]].append(IntervalTup(int(line[2]), int(line[3])))

  return m2gid2ints


def readLengthsCSV(infile: str) -> Dict[str, int]:
  """
  Read a genome lengths file.
  """
  genome2len = {}
  with open(infile, newline='') as f:
    for line in csv.reader(f):
      genome2len[line[0]] = int(line[1])

  return genome2len


def readSummaryCSV(infile: str) -> Dict[str, Dict[str, CSVInfo]]:
  """
  Read a CSV file in as a dictionary mapping methods to CSVInfos.
  """
  m2m2info: Dict[str, Dict[str, CSVInfo]] = dd(dict)
  with open(infile, newline='') as f:
    lines = deque(csv.reader(f))

    while lines:
      if lines[0][0]:
        info = CSVInfo(lines)
        m2m2info[info.m1][info.m2] = info
      else:
        lines.popleft()     # pop blank lines in between blocks

  return m2m2info


def readCSVlines(infile: str) -> List[List[str]]:
  """
  Read a CSV file in as a list of lists of strings.
  """
  with open(infile, newline='') as f:
    return list(csv.reader(f))


def readBlocksMany(blockfiles: Iterable[Path]
                   ) -> Tuple[Dict[str, Dict[str, Dict[str, List]]],
                              Set[str]]:
  """
  Read in .bed files that specify syntenic blocks, which are a set of intervals
  coming from a set of genomes.

  Parameters
  ----------
  blockfiles : Iterable[str]
      the input filenames

  Returns
  -------
  Tuple[Dict[str, Dict[str, Dict[str, List[Interval]]]], Set[str], Dict[str, int]]
      (methTOblock2gid2ints, gidset) where the first organizes
      intervals by method, then syntenic block, then genome. gidset is the set
      of all genomes encountered in the files.
  """
  methTOblock2gid2ints: Dict[str, Dict[str, Dict[str, List]]] = \
    dd(lambda: dd(lambda: dd(list)))
  gidset = set()
  for blockfile in blockfiles:
    block2gid2ints, idset = readBlocks(blockfile)

    method = getStemFromFilename(Path(blockfile))
    methTOblock2gid2ints[method] = block2gid2ints
    gidset |= idset

  return methTOblock2gid2ints, gidset
        

def readBlocks(blockfile:Path,
               byseq=False,
               strip=0
               ) -> Tuple[Dict[str, Dict[str, List[Interval]]], Set[str]]:
  """
  Read a .bed.gz (or .bed) file containing syntenic blocks. Organize them by
  block, then genome.

  Parameters
  ----------
  blockfile : str
      the .bed file
  bygenome: bool, default False
      if True, organize by genome, then block
  strip: int, default 0
      strip this many nucleotides off the end of an interval

  Returns
  -------
  Tuple[Dict[str, Dict[str, List[Interval]]], Set[str], int]
      (block2sid2ints, sidset) or
      (sid2block2ints, sidset)
  """
  if getNonZipExtension(blockfile) == '.bed':
    return readBlocksBED(str(blockfile), byseq, strip)
  elif getNonZipExtension(blockfile) == '.maf':
    return readBlocksMAF(str(blockfile), byseq, strip)
  elif getNonZipExtension(blockfile) == '.gff':
    return readBlocksBED(str(blockfile), byseq, strip)

  raise(Exception(f'Unrecognized file extension "{blockfile}".'))


def getNonZipExtension(filename) -> str:
  """
  Return the extension without .gz (if it is there).
  """
  for s in reversed(Path(filename).suffixes):
    if s != '.gz':
      return s
  
  return ''


def readBlocksMAF(blockfile:str,
                  bysid:bool=False,
                  strip=0
                  ) -> Tuple[Dict[str, Dict[str, List[Interval]]], Set[str]]:
  """
  Read a .maf.gz (or .maf) file containing syntenic blocks. Organize them by
  block, then genome.

  Parameters
  ----------
  blockfile : str
      the .bed file
  bysid: bool, default False
      if True, organize by genome, then block
  strip: int, default 0
      strip this many nucleotides off the end of an interval

  Returns
  -------
  Tuple[Dict[str, Dict[str, List[Interval]]], Set[str], int]
      (block2sid2ints, sidset) or
      (block2sid2ints, sidset)
  """
  if bysid:
    sid2block2ints: Dict[str, Dict[str, List]] = dd(ddlist)
  else:
    block2sid2ints: Dict[str, Dict[str, List]] = dd(ddlist)

  if strip:
    print(f'Stripping {strip} characters from the intervals in {blockfile}.')

  if Path(blockfile).suffix == '.gz':
    openfunc = gzip.open
  else:
    openfunc = open       #type: ignore

  sidset = set()
  with openfunc(blockfile, 'rt') as f:
    for i, multialign in enumerate(MafIO.MafIterator(f)):
      for seqrec in multialign:
        start, end = get_maf_coords(seqrec.annotations)
        if bysid:
          sid2block2ints[seqrec.id][str(i)].append(IntervalTup(start, end-strip)) #type: ignore
        else:
          sid2block2ints[seqrec.id][str(i)].append(IntervalTup(start, end-strip)) #type: ignore
        sidset.add(seqrec.id)

      if i % 200000 == 0:
        gc.collect()
      
  gc.collect()
  if bysid:
    return sid2block2ints, sidset #type: ignore
  
  return block2sid2ints, sidset #type: ignore


def get_maf_coords(maf_annotations: Dict[str, int]) -> Tuple[int, int]:
  """
  Return pythonic coordinates for intervals given the maf annotation
  coordinates.

  Notes
  -----
      This is from Afif. Negative strand intervals have inverted coordinates.

  Parameters
  ----------
  maf_annotations : Dict[str, int]
      the annotations from the SeqRec object

  Returns
  -------
  Tuple[int, int]
      (start, stop) in pythonic indices
  """
  start = maf_annotations['start']
  stop  = maf_annotations['start'] + maf_annotations['size']
  if maf_annotations['strand'] == -1:
    start = maf_annotations['srcSize'] - stop
    stop = maf_annotations['srcSize'] - maf_annotations['start']

  return start, stop


def readBlocksBED(blockfile:str,
                  byseq=False,
                  strip=0
                  ) -> Tuple[Dict[str, Dict[str, List[Interval]]], Set[str]]:
  """
  Read a .bed.gz (or .bed or .gff) file containing syntenic blocks. Organize
  them by block, then genome.

  Notes
  -----
    This can read .gff files as well, as well as others!

  Parameters
  ----------
  blockfile : str
      the .bed file
  byseq: bool, default False
      if True, organize by sequence, then block
  strip: int, default 0
      strip this many nucleotides off the end of an interval

  Returns
  -------
  Tuple[Dict[str, Dict[str, List[Interval]]], Set[str], int]
      (block2sid2ints, sidset)
  """
  if byseq:
    sid2block2ints: Dict[str, Dict[str, List]] = dd(ddlist)
  else:
    block2sid2ints: Dict[str, Dict[str, List]] = dd(ddlist)

  if strip:
    print(f'Stripping {strip} characters from the intervals in {blockfile}.')

  intervals = pybedtools.BedTool(blockfile)
  sidset = set()
  for interval in intervals:
    if strip and len(interval) > strip: #Strip characters off the end (useful 
      interval.end -= strip             # when the interval len may be off by 1)

    sidset.add(interval.chrom)
    if byseq:
      sid2block2ints[interval.chrom][interval.name].append(interval) #type: ignore
    else:
      block2sid2ints[interval.name][interval.chrom].append(interval) #type: ignore

  if byseq:
    return sid2block2ints, sidset #type: ignore

  return block2sid2ints, sidset #type: ignore


def readBlocksGFF(blockfile:str,
                  byseq=False,
                  strip=0
                  ) -> Tuple[Dict[str, Dict[str, List]], Set[str]]:
  """
  Read a .gff.gz (or .gff) file containing syntenic blocks. Organize them by
  block, then sequence.

  Notes
  -----
  - Can be done with pybedtools -- see readBlocksBED! (didn't know it at the time)
  - The ID number attribute for each entry indicates the block that it is part
    of.  Everything will be python indexed in the returned object.

  Parameters
  ----------
  blockfile : str
      the .gff file
  bysid: bool, default False
      if True, organize by sequence, then block
  strip: int, default 0
      strip this many nucleotides off the end of an interval

  Returns
  -------
  Tuple[Dict[str, Dict[str, List[Interval]]], Set[str], int]
      (block2gid2ints, gidset)
  """
  if byseq:
    sid2block2ints: Dict[str, Dict[str, List]] = dd(ddlist)
  else:
    block2sid2ints: Dict[str, Dict[str, List]] = dd(ddlist)

  if strip:
    print(f'Stripping {strip} characters from the intervals in {blockfile}.')

  sidset = set()
  for feature in gffutils.DataIterator(blockfile):    #type: ignore
    if feature.start < feature.end:
      start = feature.start - 1
      end = feature.stop - strip
    else:
      assert feature.strand == '-', f'Strand is {feature.strand} but endpoints are inverted.'
      start = feature.stop - 1
      end = feature.start - strip

    block = feature.attributes['ID'][0]
    sidset.add(feature.chrom)
    if byseq:
      sid2block2ints[feature.chrom][block].append(IntervalTup(start, end)) #type: ignore
    else:
      block2sid2ints[block][feature.chrom].append(IntervalTup(start, end)) #type: ignore

  if byseq:
    return sid2block2ints, sidset #type: ignore

  return block2sid2ints, sidset #type: ignore


def writeBlocks(blockfile:Path,
                block2sid2ints: Dict[str, Dict[str, List[Interval]]]):
  """
  Write a .bed (or .bed.gz) file containing syntenic blocks.
  """
  myopen = open
  mode = 'w'
  if blockfile.name[-3:] == '.gz':
    myopen = gzip.open          #type: ignore
    mode = 'wt'

  with myopen(blockfile, mode) as f:
    for gid2ints in block2sid2ints.values():
      for ints in gid2ints.values():
        for i in ints:
          f.write(str(i))       #type: ignore


def getStemFromFilename(filename: Path) -> str:
  for ext in BLOCK_EXTS:        #Check if it matches one of the extensions first
    if filename.name[-len(ext):] == ext:
      return filename.name[:-len(ext)]

  return Path(filename).stem


def getBEDext(filename: Path|str) -> str:
  """
  If this filename has a valid BED extension, then return it.
  """
  name = Path(filename).name
  for ext in BED_EXTS:
    if name[-len(ext):] == ext:
      return ext

  return ''


def getBLOCKext(filename: Path|str) -> str:
  """
  If this filename has a valid BLOCK extension, then return it.
  """
  name = Path(filename).name
  for ext in BLOCK_EXTS:
    if name[-len(ext):] == ext:
      return ext

  return ''


def getMethodsFromGraphFilename(filename) -> List[str]:
  names, _ = Path(filename).stem.split('_graph')
  return names.split('_VS_')