from collections import namedtuple
from collections import defaultdict as defdict
from itertools import chain
import sys
import math
import networkx as nx
from pathlib import Path
from typing import Dict, Iterable, Iterator, Optional, Set, Collection, Tuple, List, Union
from statistics import mean

from pyinclude.usefulfuncs import statusprint

from . import NODE, EDGE, Interval, M1, M2
from .graphfuncs import BiBlockGraph, getMinStarCutSet, isStarPacking, showEmptyGraph
from .graphfuncs import showGraph, getEdgeNeighborhood, getSplitGraph
from .utilities import Options, CompAnalysis, getAveOccs, getDevOccs
from .io import writeCoordsDF



CUTSET_TOO_BIG = 30


class CompInfo:
  """
  Represent a connected component in the bipartite homology block graph.

  A homology block is a collection of sets of intervals, each coming from a
  genome. Each side of the bipartite graph represents a method for block
  construction, and each node is a block inferred by the method. The edges
  are labeled by the "overlap" between the blocks, which is the sum of the 
  overlaps of the intervals from all the genomes. Edges less than a certain
  cutoff (default is 1) will not appear in the graph.
  
  This class organizes the connected component by features, such as the number
  of blocks from one genome or the other. For example, maf2synteny may call
  join many blocks into one, implying a connected compenent with a single
  node for the maf2synteny version as opposed to serveral for the other method.

  Attributes
  ----------
  nodes1 : Set[NODE]
    the nodes from method 1
  nodes2 : Set[NODE]
    the nodes from method 2
  component : nx.Graph
    the connected component
  cutset : List[EDGE]
    set by calling `getMinStarCutset()` or `setMinStarCutset()`
  """

  def __init__(self, component: nx.Graph, nodes1: Set[NODE]):
    """
    Innitialize a CompInfo.  Nodes in the component from `nodes1` will be
    assigned to `self.nodes1`, whereas the other will be `self.nodes2`.
    """
    self.nodes1: Set[NODE] = set()
    self.nodes2: Set[NODE] = set()
    self.component = component
    for node in component:
      if node in nodes1:
        self.nodes1.add(node)
      else:
        self.nodes2.add(node)

    self.TOO_BIG_TO_SHOW = 200   #Don't draw graphs wth more nodes than this.
    self.cutset: Optional[List[EDGE]] = None
    self.cutweight: int = -1


  def isStar1(self):
    return len(self.nodes1) == 1


  def isStar2(self):
    return len(self.nodes2) == 1


  def printHeavyIntersection(self):
    """
    Print the intersection of nodes from the "heavy" side of the component. The
    heavy side is the one with more nodes.
    """
    if len(self.nodes1) <= len(self.nodes2):
      nodes1, nodes2 = self.nodes1, self.nodes2
    else:
      nodes2, nodes1 = self.nodes1, self.nodes2
  
    intersection = set(nodes2)
    for n1 in nodes1:
      intersection &= set(self.component[n1])

    print(f'Heavy intersection: {len(intersection)}')


  def printWeakestLink(self):
    """
    Print the edge from the component with the lowest value.
    """
    minweight = sys.maxsize
    #minedge = None
    weights = []
    for u, v, data in self.component.edges(data=True):        #type: ignore
      weights.append(data['weight'])                          #type: ignore
  
      if data['weight'] < minweight:                          #type: ignore
        minweight = data['weight']                            #type: ignore
        #minedge = (u, v)
    
    print(f'Min weight: {minweight}  Average: {mean(weights):.2f}')


  def drawComponent(self, cutset: List[EDGE],
                    block2gid2ints1: Dict[str, Dict[str, List[Interval]]],
                    block2gid2ints2: Dict[str, Dict[str, List[Interval]]],
                    iteration=0, compnum=-1, comppref='', showcomp=-1,
                    showallcomps=True, comptoobig=sys.maxsize, suffix='',
                    forcenodelabels=False, node2label: Dict[NODE, str]={}):
    """
    Either show the component to the screen or draw it to a file depending
    on the given parameters. If `comppref` is provided, then write to a file.

    Parameters
    ----------
    cutset : List[EDGE]
        the cutset to highlight
    block2gid2ints1 : Dict[str, Dict[str, List[Interval]]]
        map block name to genome id to list of intervals
    block2gid2ints2 : Dict[str, Dict[str, List[Interval]]]
        map block name to genome id to list of intervals
    iteration : int
        the current iteration (when visiting all components)
    compnum : int, optional
        only draw this iteration, by default -1
    comppref : str, optional
        save a file with this prefix, by default ''
    showcomp : int, optional
        only show this iteration, by default -1
    showallcomps : bool, optional
        show to screen even if iteration doesn't equal showcomp, by default False
    comptoobig : int, optional
        only show components smaller than this
    suffix : str, optional
        end the filename with this, by default ''
    forcenodelabels : bool, optional
        force the nodes to be labeled
    node2label :
        label the nodes with these labels
    """
    node2heat = {}
    for n in self.nodes1:
      node2heat[n] = multiIntervalRatio(block2gid2ints1[n[1]])
    for n in self.nodes2:
      node2heat[n] = multiIntervalRatio(block2gid2ints2[n[1]])

    if comppref:
      if compnum < 0 or compnum == iteration:
        self.showComponent(f'{comppref}_{iteration}{suffix}.pdf', cutset,
                           bipartitelayout=False, nodeheat=node2heat,
                           labelnodes=(False or forcenodelabels),
                           #labelnodes=(compnum != -1),
                           comptoobig=comptoobig, node2label=node2label)
    if showallcomps:
      self.showComponent(highlight=cutset, bipartitelayout=False,
                         nodeheat=node2heat, comptoobig=comptoobig,
                         labelnodes=forcenodelabels, node2label=node2label)
    elif showcomp >= 0 and showcomp == iteration:
      self.showComponent(highlight=cutset, bipartitelayout=False,
                         nodeheat=node2heat, labelnodes=True,
                         comptoobig=comptoobig, node2label=node2label)


  def showComponent(self, filename='', highlight: Collection[EDGE]=set(),
                    gidthreshold=0, bipartitelayout=True,
                    nodeheat: Dict[NODE, float]={}, labelnodes=False,
                    comptoobig=0, node2label: Dict[NODE, str]={}):
    """
    Either show or draw the component.

    Parameters
    ----------
    filename : str, optional
        if specified, write the graph to this file instead of showing it,
        by default ''
    highlight : Collection[EDGE], optional
        a set of edges to highlight, by default set()
    gidthreshold : int, optional
        show the gids where the blocks intersect if there are less than this
        man gids to show, by default 0
    bipartitelayout : bool, optional
        use the bipartite graph layout, by default True
    nodeheat : Dict[NODE, float], optional
        color the nodes according to this value (between 0 and 1)
    comptoobig : int, optional
        show only components bigger than this. if 0 then show only if it's
        bigger than `self.TOO_BIG_TOO_SHOW`
    node2label : Dict[NODE, str], optional
        label the node according to this dictionary
    """
    if((comptoobig and len(self.component) > comptoobig) or
       (not comptoobig and len(self.component) > self.TOO_BIG_TO_SHOW)):
      showEmptyGraph(len(self.component), filename)
      return
    
    showGraph(self.component, filename, gidthreshold, highlight, self.nodes1,
              bipartitelayout, nodeheat, labelnodes, node2label)


  def getStarIgnoreSet(self) -> Tuple[Set[EDGE], int]:
    """
    Find the lowest cutoff such that all edges with at least that weight form
    a star packing.

    Notes
    -----
    Do a binary search on the cutoff.

    Returns
    -------
    List[EDGE]
        the set of edges cut from the set
    """
    cutoff = 0
    if self.isStarPacking(cutoff) != None:
      return set(), 0

    weights = sorted(set(data['weight'] for _,_, data in      #type: ignore
                     self.component.edges(data=True)))
    lb = 0
    ub = len(weights) - 1
    current = int(len(weights) / 2)
    found = False
    while not found:
      edges = self.isStarPacking(weights[current])
      if edges != None:         #Current gives a star packing.
        if current == lb:
          return self.allEdgesBut(edges), weights[current]

        ub, current = current, lb + math.floor((current-lb) / 2)

      else:
        lb = min(current + 1, ub)
        current += min(math.ceil((ub-current) / 2), ub)

    assert False, 'Should never get here!'


  def allEdgesBut(self, edges: List[EDGE]):
    """
    Return all the edges from the component except those in `edges`.
    """
    return set(self.component.edges) - set(edges)


  def getEdgesAbove(self, w):
    """
    Get the edges in `self.component` that have weight higher than `w`.
    """
    return [(u,v) for u,v, data in self.component.edges(data=True) #type: ignore
            if data['weight'] > w]                                 #type: ignore

            
  def isStarPacking(self, cutoff: int) -> Union[List[EDGE], None]:
    """
    If this component, limited to subgraph induced by the edges with weight
    greater than `cutoff`, is composed only of stars, then return those edges.
    If there is no star packing, then return None.

    Returns
    -------
    List[EDGE]
        if the remaining edges form a star packing, then return them.
    """
    keptedges = self.getEdgesAbove(cutoff)
    G = nx.edge_subgraph(self.component, keptedges)

    if isStarPacking(G):
      return keptedges
  
    return None


  def getDegreesOfSubgraph(self, nodes: List[NODE]) -> List[int]:
    return sorted(d for _, d in nx.induced_subgraph(self.component, nodes).degree)


  def getMinStarCutSet(self, verbose=False) -> List[EDGE]:
    """
    A valid assignment of blocks between two methods does not have a connected
    component that has more than one vertex in each side of the bipartite
    graph (i.e. every valid connected component is a star).
    The "star cut set" is a set of edges that must be left out in order to
    make a valid assignment (a star packing). A minimum cut set is one of
    minimum weight (edges are labeled by the number of nucleotides overlapping
    by the blocks from the two methods). We called this minimum deletion into
    disjoint stars (MDDS) in the paper.
    """
    if self.cutset == None:
      self.setMinStarCutSet(verbose)

    assert self.cutset != None
    return self.cutset


  def getMinStarCutSetWeight(self) -> int:
    """
    Get the cutset weight as set by `setMinStarCutSet`.
    """
    if self.cutset == None:
      self.setMinStarCutSet()

    assert self.cutset != None
    return self.cutweight
    #return sum(self.component.edges[edge]["weight"] for edge in self.cutset)


  def setMinStarCutSet(self, verbose=False) -> None:
    """
    See the docstring for `getMinStarCutSet()`.
    """
    self.cutset, self.cutweight = getMinStarCutSet(self.component, verbose)


  def writeCutsetNeighborhoods(self, filename: str,
                               block2gid2ints1: Dict[str, Dict[str, List[Interval]]],
                               block2gid2ints2: Dict[str, Dict[str, List[Interval]]],
                               cutoff=CUTSET_TOO_BIG):
    """
    Each edge in the cutset represents an overlap between two homology blocks.
    We output the given file information about all the blocks that overlap with
    the blocks on either end of the cut edge.
    """
    assert self.cutset != None

    if cutoff and len(self.cutset) > cutoff:
      outstr = f'Cutset is too large ({len(self.cutset)} > {cutoff}).'

    else:
      outstr = '# For each edge in the cutset, we first print the intervals\n' +\
               '# associated with each edge.  We then print all the other\n' +\
               '# intervals associated with each node of the edge.\n\n'
      for edge in self.cutset:
        outstr += f'++++++++++++++ Edge {edge} ({self.component.edges[edge]["weight"]})\n'
        nstr = getEdgeNeighborhood(self.component, edge, block2gid2ints1,
                                   block2gid2ints2)
        outstr += nstr

    with open(filename, 'w') as f:
      print(outstr, file=f)

      
  def makeSplitGraph(self,
                     block2gid2ints1: Dict[str, Dict[str, List[Interval]]],
                     block2gid2ints2: Dict[str, Dict[str, List[Interval]]]
                     ) -> Tuple[nx.Graph, Set[NODE]]:
    """
    If this component is not a star decomposition, get a version of the
    component where some nodes have been split by the intervals in the blocks
    so that there is a star decomposition; the nodes that may be splittable are
    those that have multiple intervals per genome.
    """
    return getSplitGraph(self.component, block2gid2ints1, block2gid2ints2)
    

  def nodes(self):
    return self.component.nodes


  def __len__(self):
    return len(self.component)

    
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


def analyzeComponents(G: BiBlockGraph, O: Options) -> CompAnalysis:
  """
  Analyze connected components of `G`. In a reasonable relationship, the blocks
  from the two methods will form components that are only stars.

  Parameters
  ----------
  G : nx.Graph
      The full bipartite graph relating the blocks from the two methods.
  O : Options
      The input options that dictate what to compute.

  Returns
  -------
  CompAnalysis
      Analysis of the components.
  """
  sizes, stars1, stars2, big = classifyComponents(G, G.nodes1)

  print(f'Mean component size: {mean(sizes)}')
  print(f'1 to 1     : {len(sizes) - len(stars1) - len(stars2) - len(big)}')
  print(f'Stars     <: {len(stars1)}')
  print(f'Stars     >: {len(stars2)}')
  print(f'Big        : {len(big)}')
  print(f'Big lengths: {list(map(len, big))}')

      #Deal with big components:
  ca = findCuts(G, big, O)
  ca.stars1 = len(stars1)
  ca.stars2 = len(stars2)

  return ca


def classifyComponents(G: nx.Graph, nodes1) -> Tuple[List[int], List[CompInfo],
                                                     List[CompInfo],
                                                     List[CompInfo]]:
  """
  Classify the connected components as stars or non star topologies.

  Returns
  -------
  Tuple[List[int], List[CompInfo], List[CompInfo], List[CompInfo]]
      (sizes, stars1, stars2, big) where sizes is a list of the component
      sizes, stars1 are the star components that have a single block from
      method 1, and big is a list of the non-star comoponents
  """
  sizes = []
  stars1 = []
  stars2 = []
  big: List[CompInfo] = []

  for cc in nx.connected_components(G):
    ci = CompInfo(G.subgraph(cc), nodes1)

    sizes.append(len(ci))
    if len(ci) > 2:
      if ci.isStar1():
        stars1.append(ci)
      elif ci.isStar2():
        stars2.append(ci)
      else:
        big.append(ci)

  return sizes, stars1, stars2, big


def showComponentsWithHighFreqNodes(G: BiBlockGraph, cutoff):
  """
  Show the components that have nodes (blocks) with segment frequency per
  genome larger than `cutoff`.
  """
  numgenomes = len(set(gid for gid2ints in G.block2gid2ints1.values()
                       for gid in gid2ints.keys()))

  components: Set[CompInfo] = set()
  for cc in nx.connected_components(G):
    for node in cc:
      if node[0] == M1:
        if getAveOccs(G.block2gid2ints1[node[1]], numgenomes) >= cutoff:
          components.add(CompInfo(G.subgraph(cc), G.nodes1))
          continue
      elif node[0] == M2:
        if getAveOccs(G.block2gid2ints2[node[1]], numgenomes) >= cutoff:
          components.add(CompInfo(G.subgraph(cc), G.nodes2))
          continue
      else:
        raise(Exception(f'Unexpected method: {node[0]} from {node}.'))

  statusprint(f'showing {len(components)} components...')
  for ci in components:
    node2label = {}
    for node in ci.nodes1:
      node2label[node] = f'{getAveOccs(G.block2gid2ints1[node[1]], numgenomes):.1f}'
    for node in ci.nodes2:
      node2label[node] = f'{getAveOccs(G.block2gid2ints2[node[1]], numgenomes):.1f}'

    if len(ci) > 400:
      statusprint(f'WARNING: running dot on component with {len(ci)} vertices.')

    ci.drawComponent([], G.block2gid2ints1, G.block2gid2ints2,
                     forcenodelabels=True, node2label=node2label)


def showCCForCoord(G: BiBlockGraph, method: str, chrom: str, coord: int):
  """
  Show the connected component for the block spanning the given genome
  coordinates.
  """
  if method == '1':
    block2gid2ints = G.block2gid2ints1
    nodes = G.nodes1
    methodname = M1
  else:
    block2gid2ints = G.block2gid2ints2
    nodes = G.nodes2
    methodname = M2

  for node in nodes:
    if node[0] == 'm1' and node[1] == '3':
      print(f'NODE: {node}')
      print(f'      {block2gid2ints[node[1]][chrom]}')

  for block, gid2ints in block2gid2ints.items():
    if chrom in gid2ints:
      for interval in gid2ints[chrom]:
        if interval.start < coord and interval.end >= coord:
          print(f'MATCH: {interval}     {len(gid2ints[chrom])}')
          print((methodname, block, 0) in nodes)
          nx.node_connected_component(G, (methodname, block, 0))


def printHighFrequencyNodes(G: BiBlockGraph, cutoff: float):
  """
  Print the nodes (i.e. blocks) with an expected segment frequences per genome
  greater than the cutoff.
  """

  numgenomes = len(set(gid for gid2ints in G.block2gid2ints1.values()
                       for gid in gid2ints.keys()))

  Result = namedtuple('Result', ['method', 'name', 'degree', 'ave', 'dev'])
  results: List[Result] = []
  for block, gid2ints in G.block2gid2ints1.items():
    ave = getAveOccs(gid2ints, numgenomes)
    if ave > cutoff:
      if (M1, block, 0) in G:
        degree = G.degree[(M1, block, 0)]
      else:
        degree = 0

      results.append(Result(G.m1, block, degree, ave,
                            getDevOccs(gid2ints, numgenomes)))

  for r in sorted(results, key=lambda r: r.ave, reverse=True):
    print(f'{r.method} {r.name}: {r.degree} ({r.ave:.1f}, {r.dev:.2f})')
    
  results = []
  for block, gid2ints in G.block2gid2ints2.items():
    ave = getAveOccs(gid2ints, numgenomes)
    if ave > cutoff:
      if (M2, block, 0) in G:
        degree = G.degree[(M2, block, 0)]
      else:
        degree = 0

      results.append(Result(G.m2, block, degree, ave,
                            getDevOccs(gid2ints, numgenomes)))
      
      
  for r in sorted(results, key=lambda r: r.ave, reverse=True):
    print(f'{r.method} {r.name}: {r.degree} ({r.ave:.1f}, {r.dev:.2f})')


def findCuts(G: BiBlockGraph, big: List[CompInfo], O: Options,
             splitgraph=True) -> CompAnalysis:
  """
  For each big component, calculate the min cut set and the ignore set.
  The min cut set is the minimum weight set of edges removed so that there
  are only stars left.  The ignore set is the set of edges that are below
  a certain threshold, whereby the graph without these edges is only stars.

  Parameters
  ----------
  G : BiBlockGraph
      a graph
  big : List[CompInfo]
      the components of `G` that have more than 3 vertices
  O : Options
      The input options that dictate what to compute.
  splitgraph : bool, optional
      split nodes (blocks) that have mutiple intervals per genome according
      to the blocks that overlap them from the other method

  Returns
  -------
  Tuple[float, float, int, int, int, float, float]
      (mincutset, splitmincutset, stardiff1, stardiff2, bigdiff, ignoreset,
       ignorethreshold)
      where splitmincutset is the the cutset weight after splitting nodes that
      have multiple intervals per genome (see `splitgraph`), stardiff1 is
      the number of stars with center in method 1 that were created by
      splitting, and bigdiff is the change in big components created by
      splitting.
  """
  cutset: List[EDGE] = []
  splitcutset: List[EDGE] = []
  splitcutsetw = 0
  ignoreset: List[EDGE] = []
  thresholds = []
  stardiff1 = 0
  stardiff2 = 0
  bigdiff = 0
  gid2coords1: Dict[str, List[Interval]] = defdict(list)
  gid2coords2: Dict[str, List[Interval]] = defdict(list)
  pgid2coords1: Dict[str, List[Interval]] = defdict(list)
  pgid2coords2: Dict[str, List[Interval]] = defdict(list)
  for i, ci in enumerate(big):
    print(f'---- CC {i} size: {len(ci)} = {len(ci.nodes1)} + {len(ci.nodes2)}')
    lens1 = []
    lens2 = []
    for node in ci.nodes1:
      lens1.append(getIntervalCountMean(G.block2gid2ints1[node[1]]))
    for node in ci.nodes2:
      lens2.append(getIntervalCountMean(G.block2gid2ints2[node[1]]))

    print(f'Mean # of segments per genome: {lens1} {lens2}')

    wassplit = False
    if splitgraph:                          #Try to split big components
      splitg, nodes1 = ci.makeSplitGraph(G.block2gid2ints1, G.block2gid2ints2)

      if nx.number_connected_components(splitg) > 1:
        wassplit = True
        _, stars1, stars2, newbig = classifyComponents(splitg, nodes1)
        stardiff1 += len(stars1)
        stardiff2 += len(stars2)
        bigdiff += len(newbig) - 1

        if O.bigcoords:
          for bci in newbig:
            getBigNodeCoordinates(bci, gid2coords1, gid2coords2, G)
        if O.bigcoordsper:
          for bci in newbig:
            getBigNodeCoordinates(bci, pgid2coords1, pgid2coords2, G)

        splitci = CompInfo(splitg, nodes1)    #Could be multiple components
        print(f'Split star cut set weight: {splitci.getMinStarCutSetWeight()}')
        splitcutsetw += splitci.getMinStarCutSetWeight()
        splitcutset.extend(splitci.getMinStarCutSet())
        if O.comppref or O.showallcomps or O.showcomp >= 0:
          splitci.drawComponent(splitcutset, G.block2gid2ints1,
                                G.block2gid2ints2, i, O.compnum, O.comppref,
                                O.showcomp, O.showallcomps, O.comptoobig,
                                '_split')


    #ci.printWeakestLink()
    #ci.printHeavyIntersection()
    cutset.extend(ci.getMinStarCutSet())
    print(f'Star cut set weight:       {ci.getMinStarCutSetWeight()}')
    if not wassplit:
      splitcutsetw += ci.getMinStarCutSetWeight()
      splitcutset += ci.getMinStarCutSet()
      if O.bigcoords:
        getBigNodeCoordinates(ci, gid2coords1, gid2coords2, G)
      if O.bigcoordsper:
        getBigNodeCoordinates(ci, pgid2coords1, pgid2coords2, G)

    if not O.noignoreset:
      ignset, threshold = ci.getStarIgnoreSet()
      ignoreset.extend(ignset)
      thresholds.append(threshold)

    if O.cutsetpref:
      if O.cutsetnum < 0 or O.cutsetnum == i:
        if O.cutsetnum == i:
          cutoff = 0
        else:
          cutoff = CUTSET_TOO_BIG

        ci.writeCutsetNeighborhoods(f'{O.cutsetpref}_{i}.txt',
                                    G.block2gid2ints1, G.block2gid2ints2,
                                    cutoff)

    if O.comppref or O.showallcomps or O.showcomp >= 0:
      ci.drawComponent(cutset, G.block2gid2ints1, G.block2gid2ints2, i,
                       O.compnum, O.comppref, O.showcomp, O.showallcomps,
                       O.comptoobig)

    if O.bigcoordsper:
      perfile = Path(f'{O.bigcoordsper}', f'bigcoords_{i}.csv.gzip')
      writeCoordsDF(perfile, pgid2coords1, pgid2coords2, G.m1, G.m2)


  cutsetw = sum(G.edges[edge]["weight"] for edge in cutset)
  ignoresetw = sum(G.edges[edge]["weight"] for edge in ignoreset)
  totalw = sum(data["weight"] for u,v, data in G.edges(data=True)) #type: ignore

  cutsetratio = cutsetw/totalw
  splitcutsetratio = splitcutsetw/totalw
  ignoresetratio = ignoresetw/totalw
  meanthreasholds = mean(thresholds) if thresholds else 0
  print(f'\nBig after split:       {len(big) + bigdiff}')
  print(f'Cutset weight (split): {splitcutsetratio:.6f} = {splitcutsetw}/{totalw}')
  print(f'Cutset weight:         {cutsetratio:.6f} = {cutsetw}/{totalw}')
  print(f'Ignoreset weight:      {ignoresetratio:.6f} = {ignoresetw}/{totalw}')
  print(f'       threshold:      {meanthreasholds:.2f}')
  print(f'New stars <:           {stardiff1}')
  print(f'New stars >:           {stardiff2}')
  print()

  print(f'Cutset intersection:   {len(cutsetIntersect(cutset, splitcutset))}'
        f' from {len(splitcutset)} and {len(cutset)} edges')

      #Calculate weights for cutsets that are incident to blocks with no
      # duplicated segments:
  nodupscutsetw = sum(G.edges[edge]["weight"]
                      for edge in filterDups(G, cutset))
  ndctratio = nodupscutsetw/totalw

  nodupssplitcutsetw = sum(G.edges[edge]["weight"]
                           for edge in filterDups(G, splitcutset))
  ndscsratio = nodupssplitcutsetw/totalw

  print(f'Cutset weight (split, no dups): {ndscsratio:.6f} = '
        f'{nodupssplitcutsetw}/{totalw}')
  print(f'Cutset weight (no dups):        {ndctratio:.6f} = '
        f'{nodupscutsetw}/{totalw}')
  print()

      #Calculate duplication frequencies for cutsets:
  segfreqs1, segfreqs2 = G.getSegFrequencies()
  print(f'Expected Duplicate Frequency for all Blocks/Nodes: '
        f'{mean(segfreqs1):.3f} {mean(segfreqs2):.3f} '
        f'{mean(chain(segfreqs1, segfreqs2)):.3f}')

  segfreqs1, segfreqs2 = G.getSegFrequencies(splitcutset)
  val = None
  if segfreqs1 or segfreqs2:
    val = f'{mean(chain(segfreqs1, segfreqs2)):.3f}'
  print(f'Split Cutset Duplicate Frequencies: {val}')

  segfreqs1, segfreqs2 = G.getSegFrequencies(cutset)
  val = None
  if segfreqs1 or segfreqs2:
    val = f'{mean(chain(segfreqs1, segfreqs2)):.3f}'
  print(f'Cutset Duplicate Frequencies: {val}')

  if O.bigcoords:
    writeCoordsDF(Path(O.bigcoords), gid2coords1, gid2coords2, G.m1, G.m2)

  return CompAnalysis(cutsetratio, splitcutsetratio, stardiff1, stardiff2,
                      bigdiff, ignoresetratio, meanthreasholds, len(big))


def cutsetIntersect(eset1: Iterable[EDGE], eset2: Iterable[EDGE]) -> List[EDGE]:
  """
  Compare the two sets of EDGEs, ignoring the 3rd entry of the NODE, which is
  the counter used to distinguish split notes.
  """
  m2b2other: Dict[str, Dict[str, List[NODE]]] = defdict(lambda: defdict(list))
  for u, v in eset1:
    m2b2other[u[0]][u[1]].append(v)
    m2b2other[v[0]][v[1]].append(u)

  intersection = []
  for e in eset2:
    u, v = e
    if u[0] in m2b2other and u[1] in m2b2other[u[0]]:
      for other in m2b2other[u[0]][u[1]]:
        if other and other[0] == v[0] and other[1] == v[1]:
          intersection.append(e)

  return intersection


def filterDups(G: BiBlockGraph, eset: Iterable[EDGE]) -> Iterator[EDGE]:
  """
  Yield only the edges that are not incident to a block with a duplicated
  segment in it.
  """
  for e in eset:
    if e[0][0] == M1:
      g2ints1 = G.block2gid2ints1[e[0][1]]
      g2ints2 = G.block2gid2ints2[e[1][1]]
    else:
      g2ints1 = G.block2gid2ints1[e[1][1]]
      g2ints2 = G.block2gid2ints2[e[0][1]]

    if noDups(g2ints1) and noDups(g2ints2):
      yield e


def noDups(g2ints: Dict[str, List[Interval]]) -> bool:
  """
  Return True if there are not multiple segments for any of the genomes.

  Parameters
  ----------
  g2ints : Dict[str, List[Interval]]
      map genome to a list of segments (in the block)
  """
  return all(len(v) <= 1 for v in g2ints.values())


def getBigNodeCoordinates(ci: CompInfo,
                          gid2coords1: Dict[str, List[Interval]],
                          gid2coords2: Dict[str, List[Interval]],
                          G: BiBlockGraph,
                          initialize=False):
  """
  Fill gid2coords dictionaries with intervals from the blocks.
  """
  if initialize:
    gid2coords1 = defdict(list)
    gid2coords2 = defdict(list)
    
  for n in ci.component:
    if n[0] == M1:
      for gid, ilist in G.block2gid2ints1[n[1]].items():
        gid2coords1[gid].extend(ilist)
    else:
      for gid, ilist in G.block2gid2ints2[n[1]].items():
        gid2coords2[gid].extend(ilist)


def multiIntervalRatio(gid2ints: Dict[str, List[Interval]]) -> float:
  """
  Return the proportion of the interval lists that have more than one element.
  """
  multicount = 0
  for ilist in gid2ints.values():
    if len(ilist) > 1:
      multicount += 1

  return multicount / len(gid2ints)



def getIntervalCountMean(gidTOints: Dict[str, List[Interval]]) -> float:
  """
  Return the average number of intervals per genome for the given block.

  Parameters
  ----------
  gidTOints : Dict[str, List[Interval]]
      map of genome id to interval list
  """
  lens = []
  for intervals in gidTOints.values():
    lens.append(len(intervals))

  return mean(lens)
