from typing import Tuple, Union

import pybedtools

#NCBI Entrez values that need to bet set:
EMAIL = ''
API_KEY = ''

#Types
#class NODE(NamedTuple):
#  method: str
#  block: str
#  splitid: int
NODE = Tuple[str, str, int]               #(method, block, splitnum)
EDGE = Tuple[NODE, NODE]


#Constants
NODE0: NODE = ('', '', 0)                 #the null node
M1 = 'm1'                   #The string for method 1
M2 = 'm2'                   #The string for method 2


#Intervals:
class PBTInterval(pybedtools.Interval):   #workaround for pybedtools.Interval
  def __init__(self, *args, **kwargs):
    super().__init__(args, kwargs)


class IntervalTup:
  """
  For support of MAF files.
  """
  def __init__(self, start, end, block='', method=''):
    self.start = start
    self.end = end
    self.block = block          #For use in BiBlockGraph.buildGraphFast()
    self.method = method        #For use in BiBlockGraph.buildGraphFast()

  def __len__(self):
    return self.end - self.start

    
Interval = Union[PBTInterval, IntervalTup]
