from itertools import product, tee, zip_longest
from typing import Dict, List, Tuple, Sequence

from . import Interval, IntervalTup


def getCoverage(list: List[Interval]) -> int:
  """
  Return the total coverage of the list. This is the sum of the length of the
  intervals in the list.

  Notes
  -----
    Assumes non-overlapping intervals!
  """
  return sum(i.end - i.start for i in list)


def getTotalOverlap(list1: List[Interval], list2: List[Interval]) -> int:
  """
  Return the total number of overlapping indices between the two lists.

  Notes
  -----
    Assumes that the lists are sorted.
  """
  i = 0
  j = 0
  overlap = 0
  while i < len(list1) and j < len(list2):
    i_ = i              #Find list1 intervals overlapping with list2[j] 
    j_inc = 1
    while i_ < len(list1) and list1[i_].start < list2[j].end:
      if list1[i_].end < list2[j].start:
        j_inc += 1
      else:
        overlap += overlaplenPair(list1[i_], list2[j])

      i_ += 1

    j_ = j + j_inc      #Find other list2 intervals overlapping with list1[i]
    while j_ < len(list2) and list2[j_].start < list1[i].end:
      overlap += overlaplenPair(list1[i], list2[j_])
      j_ += 1

    j += 1
    i += 1

  #assert overlap == overlapLen(list1, list2)
  return overlap


def overlapLen(ints1: Sequence[Interval],
               ints2: Sequence[Interval]) -> int:
  """
  Return the number of base pairs of overlap for the intervals from the
  two lists.

  Notes
  -----
    Use `getTotalOverlap` if the lists are long.
  """
  total = 0
  for int1, int2 in product(ints1, ints2):
    total += overlaplenPair(int1, int2)
  
  return total


def getNonOverlapping(intervals: Sequence) -> List[Interval]:
  """
  Given a list of potentially overlapping intervals, return a list of
  non-overlapping intervals that cover all the indices of the original list.

  Notes
  -----
    Assumes the list is sorted on start index!
  """
  retlist = []
  i = 0
  while i < len(intervals):
    j = i + 1
    end = intervals[i].end
    while j < len(intervals) and intervals[j].start <= end:
      end = max(end, intervals[j].end)
      j += 1

    retlist.append(IntervalTup(intervals[i].start, end))
    i = j

  return retlist


def getListOverlap(intervals: Sequence[Interval]) -> int:
  """
  Return the number of base pairs of overlap for the intervals in the list.

  Notes
  -----
    Assumes the list is sorted on start index!
  """
  overlap = 0
  i = 0
  while i < len(intervals):
    j = i + 1
    end = intervals[i].end
    while j < len(intervals) and intervals[j].start <= end:
      overlap += end - intervals[j].start
      j += 1

    i += 1

  return overlap


def sumOverlaps(gidTOints1: Dict[str, List[Interval]],
                gidTOints2: Dict[str, List[Interval]]) -> int:
  """
  Return the sum of all the overlapping intervals for the given pair of blocks.
  """
  overlap = 0
  for gid in gidTOints1:
    if gid in gidTOints2:
      overlap += overlapLen(gidTOints1[gid], gidTOints2[gid])

  return overlap


def getOverlapInfo(gidTOints1: Dict[str, List[Interval]],
                   gidTOints2: Dict[str, List[Interval]],
                   ) -> Tuple[int, List[str]]:
  """
  Return the sum of all the overlapping intervals for the given pair of blocks,
  along with the list of genomes where there is an overlap.
  """
  total = 0
  overlaplist = []
  for gid in gidTOints1:
    if gid in gidTOints2:
      if overlap := overlapLen(gidTOints1[gid], gidTOints2[gid]):
        total += overlap
        overlaplist.append(gid)

  return total, overlaplist


def removeOverlapsFromPair(gid2ints1: Dict[str, List[Interval]],
                           gid2ints2: Dict[str, List[Interval]]) -> None:
  toremove1 = []
  toremove2 = []
  for gid in gid2ints1:
    if gid in gid2ints2:
      ints1 = gid2ints1[gid]
      ints2 = gid2ints2[gid]
      if sum(len(i) for i in ints1) < sum(len(i) for i in ints2):
        ints1, ints2 = ints2, ints1           #ints1 will have more nucleotides
        tr1, tr2 = toremove2, toremove1
      else:
        tr1, tr2 = toremove1, toremove2

        #Remove any overlap:
      for i1, i2 in product(ints1, ints2):
        if overlap := overlaplenPair(i1, i2):
          if overlap == len(i1):    #i1 contained in i2
            i1.end = i1.start
          elif overlap == len(i2):  #i2 contained in i1
            i2.end = i2.start
          elif i1.start < i2.start: #trim end of i1 (the longer one)
            i1.end -= overlap
          else:                     #trip start of i1 (the longer one)
            i1.start += overlap

        #Remove empty intervals:
      removeEmpty(ints1)
      if not ints1:
        tr1.append(gid)
      removeEmpty(ints2)
      if not ints2:
        tr2.append(gid)

  for gid in toremove1:
    del gid2ints1[gid]
  for gid in toremove2:
    del gid2ints2[gid]


def removeEmpty(ints: List[Interval]):
  """
  Remove empty intervals from the list.
  """
  toremove = []
  allempty = True
  for i, interval in enumerate(ints):
    if interval.start >= interval.end:
      assert interval.start == interval.end
      toremove.append(i)
    else:
      allempty = False

  if allempty:
    ints.clear()
  else:
    for i in reversed(toremove):
      del ints[i]


def overlaplenPair(int1: Interval, int2: Interval) -> int:
  """
  Return the number of base pairs of overlap for the two intervals.
  """
  diff = min(int1.end, int2.end) - max(int1.start, int2.start)  #type: ignore
  if diff > 0:
    return diff
  
  return 0


def isSorted(gidTOints: Dict[str, List[Interval]]) -> bool:
  """
  Return True if all of the lists are sorted on start index.
  """
  for intervals in gidTOints.values():
    for int1, int2 in pairwise(intervals):
      if int1.start > int2.start:
        return False

  return True

def pairwise(iterable, wrap=False):
  "s -> (s0,s1), (s1,s2), (s2, s3), ..."
  a, b = tee(iterable)
  first = next(b, None)
  if wrap:
    return zip_longest(a, b, fillvalue=first)

  return zip(a, b)

