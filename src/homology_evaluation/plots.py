import math
import re
import sys
from typing import Dict, Iterable, List
from collections import defaultdict as dd
from itertools import product
from pathlib import Path

import matplotlib
matplotlib.use('Agg')     #To get rid of strange warning
import matplotlib.pyplot as plt
import seaborn as sns;
import pandas as pd
import numpy as np

from .io import readCSVlines, readCoordsDF, readSummaryCSV, readLengthsCSV
from pyinclude.usefulfuncs import cmp_to_key


def makeBigCoordsPlot(infile: str, outfile: str):
  """
  Make a plot showing the coordinates of the block intervals for the big
  connected components in the block graph.
  """
  coordsdf = readCoordsDF(infile)

  df = convertStartEndToLocation(coordsdf)

  fig, ax = plt.subplots(figsize=(10,5))

  if len(df):
    sns.stripplot(x='Location', y='Genome', hue='Method', data=df, ax=ax,
                  jitter=True, dodge=True, size=3.0, alpha=0.3)

    ax.set_yticklabels(ax.get_ymajorticklabels(), fontsize=3)
    ax.legend_.remove()

  fig.tight_layout()
  fig.savefig(outfile, dpi=300)


def makeBigCoordsPlotPerGenome(infile: str, lengthfile: str, outpref: str,
                               extension: str='.png'):
  """
  For each genome, make a plot showing the coordinates of the block intervals
  for the big connected components in the block graph.
  """
  coordsdf = readCoordsDF(infile)
  gid2len = readLengthsCSV(lengthfile)

  df = convertStartEndToLocation(coordsdf)
  for gid in gid2len:
    fig, ax = plt.subplots(figsize=(10,5))

    df_genome = df[df['Genome'].isin([gid])]
    if len(df_genome):
      sns.stripplot(x='Location', y='Comparison', hue='Method', data=df_genome,
                    ax=ax, jitter=True, dodge=True, size=3.0, alpha=0.3)

      ax.set_yticklabels(ax.get_ymajorticklabels(), fontsize=3)
      ax.set_xlim(None, gid2len[gid]+100)
      ax.legend_.remove()

    fig.tight_layout()
    outdir = Path(outpref)
    p = Path(outdir, f'{gid}{extension}')
    if not outdir.exists():
      outdir.mkdir(parents=True)

    print(f'saving to {p} ...')
    fig.savefig(str(p), dpi=300)
    plt.close(fig)


def convertStartEndToLocation(coordsdf: pd.DataFrame) -> pd.DataFrame:
  """
  From a DataFrame with Start and End columns, make one with only a single
  Location column that has a row for each Start and for each End.
  """
  df = coordsdf.copy()

  coordsdf.rename(columns={'Start': 'Location'}, inplace=True)
  coordsdf.drop('End', axis=1)
  coordsdf['Type'] = 'Start'
  df.rename(columns={'End': 'Location'}, inplace=True)
  df.drop('Start', axis=1)
  df['Type'] = 'End'
  
  df = pd.concat([df, coordsdf], ignore_index=True)
  return df


def makeSummaryPlot(infile: str, lengthfile: str, outfile: str):
  """
  Make a plot summarizing coverage and star information.
  """
  m2m2info = readSummaryCSV(infile)
  totallen = sum(l for l in readLengthsCSV(lengthfile).values())

  plotrows = []
  rownames = []
  for m1 in orderMethods(m2m2info):
    for m2 in orderMethods(m2m2info[m1]):
      info = m2m2info[m1][m2]

      if(info.intersection + (info.coverage1 - info.intersection) +
         (info.coverage2 - info.intersection) != info.union):
         print(f'Warning: intersection and union length mismatch. '
               f'Are there overlapping blocks?')
      plotrows.append([info.intersection,
                       info.coverage1 - info.intersection,
                       info.coverage2 - info.intersection,
                       totallen - info.union])

      rownames.append(f'{shortenLabel(m1)} | {shortenLabel(m2)}')

  df = pd.DataFrame(plotrows, index=rownames,
                    columns=['intersection', 'specific 1st', 'specific 2nd',
                             'uncovered'])
  sns.set_theme()

  #bar1 = sns.barplot(x="day",  y="total_bill", data=total, color='darkblue')
  fig, ax = plt.subplots()

  barplot = df.plot(kind='barh', stacked=True, ax=ax, legend=True)
  #barplot.set_xlim(None, 450000000)

  fontsize = 7
  ax.set_yticklabels(ax.get_ymajorticklabels(), fontsize=fontsize)
  plt.setp(ax.get_legend().get_texts(), fontsize=fontsize)

  fig.tight_layout()
  fig.savefig(outfile)
  #fig.show()




def makeSummaryPlot_old(infile: str, outfile: str):
  """
  Make a plot summarizing coverage and star information.
  """
  m2m2info = readSummaryCSV(infile)

  plotrows = []
  for m1 in orderMethods(m2m2info):
    for m2 in orderMethods(m2m2info[m1]):
      info = m2m2info[m1][m2]
      #plotrows.append([(m1, m2), 'm1', info.intercoveragerat1])
      #plotrows.append([(m1, m2), 'm2', info.intercoveragerat2])

      plotrows.append([(m1, m2), 'm1', 'coverage', info.intercoveragerat1])
      #plotrows.append([(m1, m2), 'm1', 'stars', info.sstars1])
      #plotrows.append([(m1, m2), 'm2', 'coverage', 10])
      plotrows.append([(m1, m2), 'm2', 'coverage', info.intercoveragerat2])
      #plotrows.append([(m1, m2), 'm2', 'stars', info.sstars2])

  #df = DataFrame(plotrows, columns=['mpair', 'method', 'value'])
  df = pd.DataFrame(plotrows, columns=['mpair', 'method', 'type', 'value'])
  sns.set_theme()

  g = sns.FacetGrid(df,
                    row='mpair',
                    #col='type',
                    height=1, aspect=7,
                    ylim=(0.4, 1.0), sharey=False)
  g.map(sns.barplot, 'value', 'method', order=['m1', 'm2'], stacked=True)
  g.set_titles('')
  g.set_ylabels('')

  #sns.barplot(x='ratio', y='mpair', hue='method', data=df)

  #g = sns.catplot(x='ratio', y='method', row='mpair',
  #                data=df, kind='bar') #, ci=None, aspect=.6)

  for mpair in g.axes_dict:
    g.axes_dict[mpair].set_yticklabels([f'{mpair[0]}', f'{mpair[1]}'])

  g.fig.tight_layout()
  g.savefig(outfile)
  #g.fig.show()


def makeCutsetHeatmap(infile: str, outfile: str, namemap: Dict[str, str]=None):
  """
  Make a heatmap from a CSV file containing many pairwise comparisons.
  """
  m2m2score: Dict[str, Dict[str, float]] = dd(dict)
  method1 = ''
  method2 = ''
  for line in readCSVlines(infile):
    if line[0] == 'Datasets':
      method1, method2 = line[1], line[2]

    elif line[0] == '(split) Cut ratio':
      assert method1 and method2, f'Unexpected CSV file format.'
      m2m2score[method1][method2] = float(line[1])
      m2m2score[method2][method1] = float(line[1])
      method1 = method2 = ''

  assert not method1 and not method2, f'Unexpected CSV file format.'

  methods = orderMethods(iter(m2m2score))
  matrix = dictToArray(m2m2score, methods)
  methods = shortenLabels(methods)
  if namemap:
    try:
      methods = renameMethods(methods, namemap)
    except KeyError as e:
      print(f'ERROR: missing a translation for {e} in NAME_MAP.', file=sys.stderr)
      return

  fig, ax = plt.subplots()
  sns.set_theme()
  #sns.set(font_scale=1 / math.sqrt(len(matrix)))
  annot_fontsize = 10 / math.sqrt(len(matrix))
  ticks_fontsize = annot_fontsize * 3
  annotation = {"size": annot_fontsize}
  df = pd.DataFrame(matrix, methods, methods)
  ax = sns.heatmap(df, annot=True, vmin=0, vmax=0.1, ax=ax, linewidths=1,
                   fmt='.2e', annot_kws=annotation, cmap='viridis')
  ax.set_xticklabels(ax.get_xmajorticklabels(), fontsize=ticks_fontsize)
  ax.set_yticklabels(ax.get_ymajorticklabels(), fontsize=ticks_fontsize)
  #im, _ = heatmap(matrix, methods, methods, ax=ax, vmin=0, vmax=0.1,
  #                cbarlabel="(Split) Star Score")
  #annotate_heatmap(im, threshold=0.05, size=3, valfmt='{x:.2e}')
  #                 #valfmt=matplotlib.ticker.FuncFormatter(blankFormatter))

  fig.tight_layout()
  ##plt.show()
  plt.savefig(outfile)


def blankFormatter(x, pos):
  return '{:.2e}'.format(x).replace('2.0', '')


def dictToArray(m2m2score: Dict[str, Dict[str, float]],
                labels: List[str]) -> np.ndarray:
  """
  Produce a numpy array from a 2 by 2 matrix. Use the ordering of the labels
  given by `labels`.
  """
  index = {method: i for i, method in enumerate(labels)}

  m = np.full((len(labels), len(labels)), np.nan)
  for l1, l2 in product(labels, repeat=2):
    if l1 in m2m2score and l2 in m2m2score[l1]:
      m[index[l1]][index[l2]] = m2m2score[l1][l2]

  return m


abbrev = {'nooutgroup': 'nout',
          'filtered': 'fil',
          'maf2synteny': 'm2s'}
def shortenLabel(label: str) -> str:
  for old, new in abbrev.items():
    label = label.replace(old, new)

  return label

  
def shortenLabels(labels: List[str]) -> List[str]:
  newlabels = []
  for l in labels:
    newlabels.append(shortenLabel(l))

  return newlabels
    

def renameMethods(methods: List[str], namemap: Dict[str, str]) -> List[str]:
  return [namemap[method] for method in methods]


def orderMethods(methods: Iterable[str]) -> List[str]:
  """
  Sort the method names into an order that makes them easy to compare.
  """
  return sorted(methods, key=cmp_to_key(compareMethods))


    #A name gets the priority of the last matching member:
priority = ['_R', '-nooutgroup', '-nooutgroup_R',  '-nooutgroup-filtered',
            '+maf2synteny', '+maf2synteny_R', '-nooutgroup+maf2synteny',
            '-nooutgroup-filtered+maf2synteny']
suffixre = re.compile(f'({"|".join(re.escape(p) for p in priority)})')
def compareMethods(m1, m2):
  parts = suffixre.split(m1, maxsplit=1)
  base1 = parts[0]
  rest1 = ''
  if len(parts) > 1:
    rest1 = parts[1] + parts[2]
  parts = suffixre.split(m2, maxsplit=1)
  base2 = parts[0]
  rest2 = ''
  if len(parts) > 1:
    rest2 = parts[1] + parts[2]

  priority.insert(0, '')
  if base1 == base2:
    if rest1 == rest2:
      return 0
    elif priority.index(rest1) < priority.index(rest2):
      return -1
    else:
      return 1

  elif base1 < base2:
    return -1

  return 1
