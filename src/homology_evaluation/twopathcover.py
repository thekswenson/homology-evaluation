import networkx as nx
import numpy as np
from typing import List, Collection, Tuple

from . import NODE, EDGE, NODE0


def choose2PathCover(forest: nx.Graph, path: Collection[NODE]) -> List[EDGE]:
  """
  Given a forest of trees and a path in the forest (of degree 2 nodes),
  return the heaviest of the 2-path covers. A 2-path cover is a vertex disjoint
  cover of the vertices of `path`, including edges that are hanging off the end
  of the path (these edges connect the path to either a star center or a 
  leaf node in the forest) by paths of length 0, 1, and 2. An end vertex that
  touches a star is a special in that it can only be part of a length 1 path.

  Parameters
  ----------
  forest : nx.Graph
      the trees from which the path was extracted
  path : Collection[NODE]
      the collection of nodes that from a path of degree 2 nodes in the forest

  Returns
  -------
  List[EDGE]
      return the edges that should be remove from the forest to ensure the given
      path participates only in stars.
  """
  pathG = nx.induced_subgraph(forest, path)
  numedges = len(path) + 1

  w, edges, dfirst, dlast = getWeightArray(forest, pathG)
  Dm = np.empty(numedges)      #Excluding the ith edge (m is for minus).
  Dp = np.empty(numedges)      #Including the ith edge (p is for plus).
  Dm_from = np.empty(numedges, dtype=int) #The Dp subproblem used for Dm[i].
  Dp_from = np.empty(numedges, dtype=int) #The Dm subproblem used for Dp[i].

    #Calculate the score of the best solution:
    # Dp[i] is the score of the subproblem for the interval 0..i (inclusive)
    # such that the ith edge is taken. Dm[i] is the same such that the edge i
    # is not taken.
    # Dp_from (Dm_from) is the index from which the next subproblem is taken
    # when calculating Dp (Dm).
    #
    #The first two act differently:
  Dm[0] = 0             #weight without first edge
  Dm_from[0] = -1
  Dp[0] = w[0]          #weight with first edge
  Dp_from[0] = -1

  Dm[1] = Dp[0]
  Dm_from[1] = -1

  if dfirst == 1:
    if numedges == 2 and dlast > 1: #For a length 2 path with a star at the end
      Dp[1] = w[1]                  #we can't take w[0].
      Dp_from[1] = 0
    else:
      Dp[1] = w[0] + w[1] #take both edges
      Dp_from[1] = -1

  else:                 #first vertex touches star
    Dp[1] = w[1]        #since we have to take this one,
    Dp_from[1] = 0      #the first vertex is not taken

    #The i > 1 act mostly the same (except for the last edge, sometimes)
  i = 2
  while i < numedges:
    #Dm[i] == max(Dp[i-1], Dp[i-2])
    Dm_from[i] = i - (2 - np.argmax(Dp[i-2:i]))
    Dm[i] = Dp[Dm_from[i]]
    assert Dm[i] == max(Dp[i-1], Dp[i-2])

    #Dp[i] = max(w[i] + Dm[i-1],
    #            w[i] + w[i-1] + Dm[i-2])
    dp1 = w[i] + Dm[i-1]            #take only the last edge
    dp2 = w[i] + w[i-1] + Dm[i-2]   #take the last two edges
    if dp1 >= dp2 or (i == numedges-1 and dlast > 1):
      Dp_from[i] = i-1              #if the last edge participates in a star
      Dp[i] = dp1                   #then we can only take one edge
    else:
      Dp_from[i] = i-2
      Dp[i] = dp2
    assert Dp[i] == max(w[i] + Dm[i-1],  w[i] + w[i-1] + Dm[i-2]) or i == numedges-1

    i += 1

    #Reconstruct the solution:
  return getRemovedEdges(edges, Dp, Dm, Dp_from, Dm_from)

  
def getRemovedEdges(path: List[EDGE], Dp, Dm, Dp_from, Dm_from) -> List[EDGE]:
  """
  Trace back through the DP to find the edges that are not in the star packing
  on the path.
  """
  i = len(Dm) - 1
  remedges = []
  useDp = False
  weight = 0              #weight of the ignored edges
  if Dp[i] >= Dm[i]:                  #last edge is taken
    useDp = True

  while i > 1:
    if useDp:                         #i'th edge is taken
      i = Dp_from[i]

    else:                             #i'th edge skipped
      for j in reversed(range(Dm_from[i]+1, i+1)):
        remedges.append(path[j])      #skipping these edges
      i = Dm_from[i]

    useDp = not useDp

  if i == 1:              #if i == 1 and useDp is True
    if useDp:             #then we may use 0th edge too
      i = Dp_from[i]      #depending on Dp_from[1].
      useDp = not useDp
    else:
      remedges.append(path[1])

  if i == 0 and not useDp:
    remedges.append(path[0])

  remedges.reverse()
  return remedges


def getWeightArray(forest: nx.Graph,
                   path: nx.Graph) -> Tuple[np.ndarray, List[EDGE], int, int]:
  """
  Given a path of degree 2 nodes from the given `forest`, return a weight
  array for every edge of the path, including the edges at the end of the path
  that share a node with the forest.

  Returns
  -------
  Tuple[np.ndarray, List[EDGE], int, int]
      (w, l, d1, d2) where w[i] has the weight of the ith edge in the path l
      (the weight of l[i]), and d1 is the degree (in the forest) of the first
      node of the returned path l and d2 is the degree (in the forest) of the
      last node of the path.
  """
    #Get one of the end nodes of the path:
  start = NODE0
  for v in path:
    if path.degree[v] <= 1:
      start = v
      break
  
    #Get the edge hanging off of the path (either a star center or a leaf):
  nodei = getNext(path, start, NODE0)         #next node on the path
  extremity1 = getNext(forest, start, nodei)

  w = np.empty(len(path) + 1)
  l = []

  w[0] = forest[extremity1][start]['weight']  #weight with extremity1
  l.append((extremity1, start))

  prev = start
  i = 1
  nodei_next = nodei
  while nodei_next != NODE0:
    w[i] = forest[prev][nodei]['weight']
    l.append((prev, nodei))

    nodei_next = getNext(path, nodei, prev)
    i += 1
    if nodei_next != NODE0:
      prev = nodei
      nodei = nodei_next

  if nodei == NODE0:     #for a path with a single node
    nodei = start
    prev = extremity1

  extremity2 = getNext(forest, nodei, prev)
  w[i] = forest[nodei][extremity2]['weight']
  l.append((nodei, extremity2))

  return w, l, forest.degree[extremity1], forest.degree[extremity2]


def getNext(path: nx.Graph, current: NODE, prev: NODE) -> NODE:
  """
  Get the neighboring node to `current` that is `prev`. If `prev` is the null
  node NODE0, then return either neighbor. If there is no neighbor matching
  the criterion, then return the null node NODE0.
  """
  for n in path.neighbors(current):
    if n != prev:
      return n

  return NODE0   #no next node in the path


def getFullPath(forest: nx.Graph, path: Collection[NODE]) -> List[EDGE]:
  """
  Given a path of degree-2 nodes from `forest`, return the path with the two
  edges hanging off the ends of it.
  """
  pathG = nx.induced_subgraph(forest, path)
    #Find an end of the path:
  v = NODE0
  for v in path:
    if pathG.degree[v] == 1:
      break

    #Add the part hanging off the beginning:
  nextv = getNext(pathG, v, NODE0)
  retpath = [(getNext(forest, v, nextv), v)]

  prev = v
  if nextv == NODE0:      #length 2 path
    prev = retpath[0][0]
    nextv = v
  else:
    retpath.append((prev, nextv))

    #Add the rest (assumes the path has no degree > 2 nodes in the forest):
  while nextv != NODE0 and forest.degree[nextv] <= 2:
    prev, nextv = nextv, getNext(forest, nextv, prev)
    if nextv != NODE0:
      retpath.append((prev, nextv))

  return retpath

