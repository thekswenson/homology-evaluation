"""
Assortment of functions.
"""
import pybedtools

from typing import Dict, List, Tuple, Sequence, Union
from collections import defaultdict as dd
from itertools import chain
from multiprocessing import Pool
from statistics import mean, stdev

from .intervals import IntervalTup, getTotalOverlap, getNonOverlapping
from .intervals import getCoverage
from . import Interval


#______________________________________________________________________________#
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|   OBJECTS   |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#


class CompAnalysis:
  """
  Hold information about analyzed components.

  Attributes
  ----------
  stars1 : int
    the number of stars that have a single vertex in genome 1
  stars2 : int
    the number of stars that have a single vertex in genome 2
  big : int
    the number of components that aren't stars or triangles
  cutratio : float
    the ratio of the weight of the min cut set to the weight of the entire
    graph. a cut set is a set of edges the removal of which leaves only stars.
  splitcutratio : float
    the same as cutratio, but after splitting the nodes with multiple intervals
    for a genome
  stardiff1 : int
    the number of stars1 created by spitting big components
  stardiff2 : int
    the number of stars2 created by spitting big components
  bigdiff : int
    the number of big created/destroyed by the splitting process
  ignoreratio : float
    the ratio of the weight of the min ignore set to the weight of the entire
    graph. an ignore set is the set of all edges under a certain threshold,
    such that the remainder of the graph is only stars.
  threshold : float
    the average threshold used for ignoreratio
  """
  def __init__(self, cutratio: float, splitcutratio: float, stardiff1: int,
               stardiff2: int, bigdiff: int, ignoreratio: float,
               threshold: float, big: int, stars1=-1, stars2=-1):
    self.stars1: int = stars1
    self.stars2: int = stars2
    self.big: int = big
    self.cutratio: float = cutratio
    self.splitcutratio: float = splitcutratio
    self.stardiff1: int = stardiff1
    self.stardiff2: int = stardiff2
    self.bigdiff: int = bigdiff
    self.ignoreratio: float = ignoreratio
    self.threshold: float = threshold


  def initialized(self) -> bool:
    return self.stars1 >= 0 and self.stars2 >= 0


  def splitstars1(self) -> int:
    assert self.initialized()
    return self.stars1 + self.stardiff1


  def splitstars2(self) -> int:
    assert self.initialized()
    return self.stars2 + self.stardiff2


  def splitbig(self) -> int:
    return self.big + self.bigdiff


class Options:
  def __init__(self, compnum=-1, comppref='', showcomp=-1, showallcomps=False,
               cutsetpref='', cutsetnum=-1, noignoreset=False, bigcoords='',
               bigcoordsper='', comptoobig=0):
    """
    Make it easy to pass program options between functions.

    Parameters
    ----------
    compnum : int, optional
        Create a PDF of a connected component, by default -1
    comppref : str, optional
        Create PDFs of the componenets with filename prefixes like this.
    showcomp : int, optional
        Print the showcompth connected component to the screen, by default -1
    showallcomps : bool, optional
        Print all the components to the screen, by default False
    cutsetpref : str, optional
        Write information about a cutset to this path
    cutsetnum : int, optional
        Write cutset information for only this component
    noignoreset : bool, optional
        do not compute the ignore set
    bigcoords : str, optional
        write the big component coordinates to this csv file
    bigcoordsper : str, optional
        write a big component coordinates file, per component, to this directory
    comptoobig : int, optional
        do not draw components that are bigger than this
    """
    self.compnum = compnum
    self.comppref = comppref
    self.showcomp = showcomp
    self.showallcomps = showallcomps
    self.cutsetpref = cutsetpref
    self.cutsetnum = cutsetnum
    self.noignoreset = noignoreset
    self.bigcoords = bigcoords
    self.bigcoordsper = bigcoordsper
    self.comptoobig = comptoobig


#______________________________________________________________________________#
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|  FUNCTIONS  |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#


def getOverlapStats(block2gid2ints1: Dict[str, Dict[str, List[Interval]]],
                    block2gid2ints2: Dict[str, Dict[str, List[Interval]]],
                    threads=1) -> Tuple[int, int, int, int]:
  """
  Get the sum of the overlapping intervals in all the genomes, as well as the
  total number of nucleotides covered by both methods, and the total coverage
  for each method.

  Parameters
  ----------
  block2gid2ints1 : Dict[str, Dict[str, List[Interval]]]
      map of the block to the genome to the intervals for that pair
  block2gid2ints2 : Dict[str, Dict[str, List[Interval]]]
      map of the block to the genome to the intervals for that pair
  threads : int, optional
      use this many threads, by default 1

  Returns
  -------
  Tuple[int, int, int, int]
      (intersection, union, coverage1, coverage2)
  """
  gidTOints1 = blockBasedToSeqBased(block2gid2ints1)
  gidTOints2 = blockBasedToSeqBased(block2gid2ints2)

  #assert isSorted(gidTOints1)
  #assert isSorted(gidTOints2)

  intersection = 0
  union = 0
  coverage1 = 0
  coverage2 = 0
  if threads > 1:
    with Pool(processes=threads) as pool:
      results = pool.starmap_async(getOverlapStatsPerGenome,
                                   ((gidTOints1[gid], gidTOints2[gid])
                                    for gid in gidTOints1)).get()

      for i, u, c1, c2 in results:
        intersection += i
        union += u
        coverage1 += c1
        coverage2 += c2
  else:
    for gid in gidTOints1:
      i, u, c1, c2 = getOverlapStatsPerGenome(gidTOints1[gid], gidTOints2[gid])
      intersection += i
      union += u
      coverage1 += c1
      coverage2 += c2

  return intersection, union, coverage1, coverage2


def getOverlapStatsPerGenome(ints1: Sequence[Union[Interval, IntervalTup]],
                             ints2: Sequence[Union[Interval, IntervalTup]]
                             ) -> Tuple[int, int, int, int]:
  """
  Get the number of nucleotides coverted by both lists of intervals, as well as
  the total coverage for the union of the lists, and each individual list.
  """
  nonoverlapping1 = getNonOverlapping(ints1)
  nonoverlapping2 = getNonOverlapping(ints2)
  nonoverlapping = getNonOverlapping(sorted(chain(ints1, ints2), key=lambda x: x.start))

  return (getTotalOverlap(nonoverlapping1, nonoverlapping2),
          getCoverage(nonoverlapping),
          getCoverage(nonoverlapping1),
          getCoverage(nonoverlapping2))


def getTotalGenomeLength(block2gid2ints: Dict[str, Dict[str, List[Interval]]])\
  -> Tuple[int, Dict[str, int]]:
  """
  Return the total number of nucleotides in all genomes.  This is estimated
  using the block with highest index for each genome.

  Parameters
  ----------
  block2gid2ints : Dict[str, Dict[str, List[Interval]]]
      map block to genome to intervals
  """
  gid2max = dd(int)

  for gid2ints in block2gid2ints.values():
    for gid, intlist in gid2ints.items():
      for i in intlist:
        gid2max[gid] = max(i.end, gid2max[gid])

  return sum(l for l in gid2max.values()), gid2max


def genomeLensFromBlocks(blockfile:str) -> Dict[str, int]:
  """
  Read a .bed.gz (or .bed) file containing syntenic blocks. Return a dictionary
  mapping genome to the highest index of any block in that genome.

  Parameters
  ----------
  blockfile : str
      the .bed file
  """
  gid2len: Dict[str, int] = dd(int)
  for interval in pybedtools.BedTool(blockfile):
    gid2len[interval.chrom] = max(gid2len[interval.chrom], interval.end)

  return gid2len


def blockBasedToSeqBased(block2sid2ints: Dict[str, Dict[str, List[Interval]]],
                         sorted=True) -> Dict[str, List[Interval]]:
  """
  Organize Intervals by sequence.

  Parameters
  ----------
  block2sid2ints : Dict[str, Dict[str, List[Interval]]]
      the Intervals organized by syntenic block
  sorted : bool, optional
      if True, sort the intervals by start coordinate, by default True

  Returns
  -------
  Dict[str, List[Interval]]
      Map SID to (potentially sorted) list of all the intervals of that genome.
  """
  sidTOints: Dict[str, List[Interval]] = dd(list)

  for sTOis in block2sid2ints.values():
    for sid, intervals in sTOis.items():
      sidTOints[sid] += intervals
  
  if sorted:
    for intervals in sidTOints.values():
      intervals.sort(key=lambda i: i.start)   #type: ignore

  return sidTOints


def mymean(l: List, fillsize: int=0):
  if fillsize:
    return mean(l + [0] * (fillsize - len(l)))

  return mean(l)
  #if len(l) > 1:
  #  return mean(l)
  #else:
  #  return l[0]


def mystdev(l: List, fillsize: int=0):
  if fillsize:
    return stdev(l + [0] * (fillsize - len(l)))

  return stdev(l)


def getAveOccs(gid2ints: Dict[str, List[Interval]], numgenomes=0):
  """
  Return the average number of intervals per genome.

  Parameters
  ----------
  numgenomes : int, optional
      fill in with zeros if this number of genomes are not present
  """
  return mymean([len(v) for v in gid2ints.values() if len(v)], numgenomes)


def getDevOccs(gid2ints: Dict[str, List[Interval]], numgenomes=0):
  """
  Return the standard deviation of the number of intervals per genome.

  Parameters
  ----------
  numgenomes : int, optional
      fill in with zeros if this number of genomes are not present
  """
  return mystdev([len(v) for v in gid2ints.values() if len(v)], numgenomes)
