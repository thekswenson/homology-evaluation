"""
This module contains globals used with multiprocessing to avoid copying memory.
In Linux this yields significant memory gains.
"""
from typing import Dict, List

from . import Interval

block2gid2ints1: Dict[str, Dict[str, List[Interval]]] = {}
block2gid2ints2: Dict[str, Dict[str, List[Interval]]] = {}
