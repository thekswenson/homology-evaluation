"""
The classes to represent Genomes that are composed chromosomes with markers.
"""

from pathlib import Path
import re
from typing import Dict, List, Mapping, Optional


class Marker:
  """
  A Marker has a name, an orientation, and potentially start and end positions.
  """
  def __init__(self, fullname: str,
               start: Optional[int]=None,
               end: Optional[int]=None):
    if fullname[0] == '-':
      self.name = fullname[1:]
      self.ispositive = False
    elif fullname[0] == '+':
      self.name = fullname[1:]
      self.ispositive = True
    else:
      self.name = fullname
      self.ispositive = True

    self.start = start
    self.end = end

  def __str__(self):
    return f'{"+" if self.ispositive else "-"}{self.name}'


class Chromosome(List[Marker]):
  """
  A Chromosome is a list of Markers and it can be linear or circular.
  """
  _commentre = re.compile(r'\/\/.*$')
  _chromre = re.compile(r'\/\/\s*(\S+)\s+(\S+)')
  _LINEAR = '|'
  _CIRCULAR = ')'

  def __init__(self, cid: str='',
               name: str='',
               linear: bool=True,
               *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.name = name
    self.cid = cid
    self.linear = linear

  def fromStr(self, chromstr: str):
    """
    Read a chromosome from a line in UNIMOG format.
    """
    chrom = sid = ''
    remains = chromstr.strip()
    if m := self._commentre.search(chromstr):
      if m := self._chromre.match(m.group()):
        chrom, sid = m.groups()     #get chrom name and id from comment
      remains = self._commentre.sub('', chromstr).strip()

    if sid:
      self.cid = sid
    if chrom:
      self.name = chrom

    if not remains:
      return False      #no chromosome information

    if remains[-1] == self._LINEAR:
      self.linear = True
    elif remains[-1] == self._CIRCULAR:
      self.linear = False
    else:
      raise(Exception(f'Linear/circular information must end line:'+
                      f'\n"{remains}"\n'
                      f'Expected "{self._LINEAR}" or "{self._CIRCULAR}".'))

    if remains[:-1]:
      #print([s for s in re.split(r'\s+', remains[:-1])])
      self.extend(Marker(s) for s in re.split(r'\s+', remains[:-1].strip()))
    else:
      raise(Exception(f'Missing marker information in line:\n"{chromstr.strip()}"'))

    return True

  def __str__(self):
    return ' '.join(str(m) for m in self) +\
           f' {self._LINEAR if self.linear else self._CIRCULAR}' +\
           f' {"// "+self.name+" "+self.cid if self.cid else ""}'


class Genome(Dict[str, Chromosome]):
  """
  A Genome is a dictionary mapping chromsome ids to Chromosomes.
  """
  def __init__(self, name='', *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.name = name

  def __str__(self):
    return f'>{self.name}\n' + '\n'.join(f'{c}' for c in self.values())


class GenomeDict(Dict[str, Genome]):
  """
  A dictionary mapping genome names to Genomes.
  """
  def __init__(self, markersfile: Optional[Path]=None, *args, **kwargs):
    super().__init__(*args, **kwargs)
    if markersfile:
      self.readMarkerFile(markersfile)

  def readMarkerFile(self, markerfile: Path):
    """
    Fill this Genome with information from a markerfile.
    """
    with open(markerfile) as f:
      nextchrom = 1
      genome = Genome()
      genomename = ''
      for line in f:
        if line.startswith('>'):        #new genome
          genomename = re.sub(r'\/\/.+', '', line[1:]).strip()
          genome = Genome(genomename)
          self[genomename] = genome

        else:                           #new chromosome
          chrom = Chromosome()
          if chrom.fromStr(line):
            assert genome.name, f'genome name line missing before first chromsome.'

            if not chrom.cid:
              if not chrom.name:
                sid = str(nextchrom)
                nextchrom += 1
              else:
                sid = chrom.name
            else:
              sid = chrom.cid

            assert sid not in genome, f'Chromosome "{sid}" already seen.'
            genome[sid] = chrom

  def __str__(self):
    return '\n'.join(f'{g}' for g in self.values())