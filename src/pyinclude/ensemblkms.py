#!/usr/bin/env python3
# Krister Swenson                                                  Fall 2012
#
# Library for getting info from ENSEMBL using pycogent.
#
#


import os
import sys

from cogent.db.ensembl import HostAccount
from cogent.db.ensembl import Genome



#______________________________________________________________________________#
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|   OBJECTS   |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#


#______________________________________________________________________________#
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|  FUNCTIONS  |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#


def getDBaccount():
  """
  Get an account from cogent.
  """
  if 'ENSEMBL_ACCOUNT' in os.environ:
    host, username, password = os.environ['ENSEMBL_ACCOUNT'].split()
    return HostAccount(host, username, password)
  else:
    sys.exit("Error: -l given but environment variable ENSEMBL_ACCOUNT is"+\
             "not set!\n\n")



def findGene(account, release, spec, gene):
  """
  Search for the given species and gene.
  """
  genome = Genome(Species=spec, Release=release, account=account)
  if genome.Species != 'None':
    print(genome)
    for g in genome.getGenesMatching(Symbol=gene):
      print(g)
    for g in genome.getGenesMatching(StableId=gene):
      print(g)
    print("__________________________________________________________________")
    print("Searching Descriptions...")
    for g in genome.getGenesMatching(Description=gene):
      print(g)
  else:
    print(('Species "'+spec+'" not found.\n'))
