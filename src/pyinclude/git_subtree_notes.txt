https://www.atlassian.com/git/tutorials/git-subtree

# Setup
# !Note that you have to add the remote each time you clone the project!
git remote add -f pyinclude git@bitbucket.org:thekswenson/pyinclude.git
git subtree add --prefix src/pyinclude/ pyinclude master --squash

# ALWAYS commit the subtree changes separately!
git subtree push --prefix=src/pyinclude/ pyinclude master
git subtree pull --prefix=src/pyinclude/ pyinclude master --squash
