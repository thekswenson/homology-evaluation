#!/usr/bin/env python3
# Krister Swenson                                                  Winter 2014
#
"""
Computes the minimum non-crossing colored partition.
 
 (MNCP) Minimum Non-Crossing Colored Partition
 INPUT: Ordered set S and color function c : S -> N.
 
 OUTPUT:
 Partition P of S such that for any two subsets s1, s2 in P we have
   if p < q < p' < q' (where p, p' in s1 and q, q' in s2) then s1 = s2, and
   if p, p' in si, then c(p) = c(p'), for all si.
 
 MEASURE: The cardinality of P.

The color function is represented as a space-delimited word.
So the input "b a b c a d a c b" implies c(1) = b, c(2) = a, c(3) = b, and so
on.
"""
import sys
import numpy

from itertools import combinations
from typing import Any, Collection, List, Set, Tuple



#______________________________________________________________________________#
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|   OBJECTS   |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#


#______________________________________________________________________________#
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|  FUNCTIONS  |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#

def mncp(w) -> List[Set]:
  """
  Given a word representing a color function, compute the minimum non-crossing
  colored partition.

  @param w: The word representing the color function.
  @type w:  list
  @retval:  The partition.
  @rtype:   list of sets.
  """
     #Find the intervals that have endpoints with matching colors.
  intervals = [i_j for i_j in combinations(enumerate(w),2)
               if i_j[0][1] == i_j[1][1]]
  intervals.sort(key=lambda i_j1: i_j1[1][0]-i_j1[0][0])

     #Score the intervals:
  blocks = [[None]*len(w) for i in range(len(w))] #Block sets for the MNCPs.
  MNCP = numpy.zeros(shape=(len(w),len(w)))       #Values for the MNCPs.
  for ((i,l1),(j,l2)) in intervals:
    MNCP[i][j], blocks[i][j] = mncpR(i, j, w, MNCP, blocks)
    
     #Score the whole thing:
  return mncpR(0,len(w)-1,w,MNCP,blocks)[1]



def mncpR(i, j, c, MNCP, blocks) -> Tuple[Any, List[Collection]]:
  """
  This is the recursive function to compute MNCP on the interval [i,j].

  @param i:      start index.
  @param j:      end index.
  @param c:      color function.
  @param MNCP:   cached values of mncpR on smaller intervals.
  @param blocks: cached list of indices for mncpR on smaller intervals.
  """
  N = [0]*len(c)       #N[x] holds the best MNCP score for the interval [i,x].
  N[i] = 1
  B: List[Collection] = [None]*len(c) #B[x] holds the block list for the best MNCP over [i,x].
  B[i] = [set([i])]                   #Each entry is a list of sets.
  for x in range(i+1,j+1):
    if c[x] == c[i]:
      n1 = N[x-1]
      b1 = []
      for s in B[x-1]: #Add the current index to the set with i:
        if i in s:
          b1.append(s | set([x]))
        else:
          b1.append(set(s))
    else:
      n1 = N[x-1]+1
      b1 = list(B[x-1])
      b1.append(set([x]))

    n2 = sys.maxsize    #The min mncp value so far over all k.
    b2 = []            #The min block set for the interval.
    for k in range(i+1, x): 
      if c[k] == c[x]:
        if n2 > N[k-1] + MNCP[k][x]:
          n2 = int(N[k-1] + MNCP[k][x])
          b2 = list(B[k-1])
          b2.extend(blocks[k][x])
    if n1 < n2:
      N[x] = n1
      B[x] = b1
    else:
      N[x] = n2
      B[x] = b2

  assert N[j] == len(B[j])
  return N[j], B[j]
