# coding: utf-8

import sys
import shutil

from itertools import chain
from pathlib import Path

# Switches:
BED_EXT = '.bed.gz'      # Change to ".bed" to disable gzipping.

# Constants:
LOG_DIR = 'maftobedlogs'

MAF_EXT = '.maf'
MAFGZ_EXT = '.maf.gz'

SRCDIR = srcdir('../src')
EXPORT_PYTHONPATH = f'export PYTHONPATH="{SRCDIR}"'


# Arguments:
#configfile: 'snakeconfig/maftobed_default.yaml'

if 'INDIR' not in config or 'OUTDIR' not in config:
  sys.exit(f'Specify INDIR and OUTDIR with\n\t"--config INDIR=/path/to/in/dir OUTDIR=/path/to/out/dir".')
IN_DIR = str(Path(config['INDIR']))
OUT_DIR = str(Path(config['OUTDIR']))

CUT_OFF = config['CUTOFF'] if 'CUTOFF' in config else 0


def buildTargetList(wildcards):
  """
  Build the list of ultimate targets based on the current set of constants.
  """
  names, = glob_wildcards(IN_DIR+'/{name}'+MAF_EXT)
  gznames, = glob_wildcards(IN_DIR+'/{name}'+MAFGZ_EXT)

  return expand(OUT_DIR+'/{name}'+BED_EXT, name=chain(names, gznames))


#-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-
# Targets
#-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-


rule all:
  input:
    buildTargetList


#-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-
# Other Rules
#-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-

def zippedORnot(wildcards):
  maf = Path(IN_DIR, '{wildcards.name}'+MAF_EXT)
  if maf.exists():
    return str(maf)

  return f'{IN_DIR}/{wildcards.name}{MAFGZ_EXT}'


rule maf_to_bed:
  """
  Convert a MAF file to a BED file.
  """
  input:
    zippedORnot

  output:
    OUT_DIR+'/{name}'+BED_EXT

  log:
    LOG_DIR+'/{name}_log.txt'

  shell:
    "{EXPORT_PYTHONPATH}; bin/convertblockfile -c {CUT_OFF} '{input}' '{output}'"
    " >& {log}"