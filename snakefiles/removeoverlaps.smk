# coding: utf-8

import sys
import shutil

from itertools import chain
from pathlib import Path

# Required values
IN_DIR = str(Path(config['IN_DIR']))
GRAPHS_DIR = str(Path(config['GRAPHS_DIR'])) #Can be modified with STRIP_LEN

# Constants:
CLEANOVERLAPS = 'bin/cleanoverlaps'
BEDZ_EXT = '.bed.gz'
BED_EXT = '.bed'

SRCDIR = srcdir('../src')
EXPORT_PYTHONPATH = f'export PYTHONPATH="{SRCDIR}"'


STRIP_LEN = int(config['STRIP_LEN']) if 'STRIP_LEN' in config else 0
if STRIP_LEN:                                #Add a suffix with STRIP_LEN
  GRAPHS_DIR += f'_{STRIP_LEN}'

SPECIFIC_PAIRS = config['SPECIFIC_PAIRS'] if 'SPECIFIC_PAIRS' in config else []
MAX_PROCESSORS = int(config['MAX_PROC']) if 'MAX_PROC' in config else 1


def buildTargetList(wildcards):
  """
  Build the list of ultimate targets based on the current set of constants.
  """
  if SPECIFIC_PAIRS:
    filelist = []
    for i, j in SPECIFIC_PAIRS:
      assert i == j, f'expected an identity file'
      filelist.append(f'{IN_DIR}/{i}_R{BEDZ_EXT}')

    return filelist
  else:
    names, = glob_wildcards(IN_DIR+'/{name}'+BED_EXT)
    znames, = glob_wildcards(IN_DIR+'/{name}'+BEDZ_EXT)

    return expand(IN_DIR+'/{name}_R'+BEDZ_EXT, name=chain(names, znames))


#-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-
# Targets
#-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-


rule all:
  input:
    buildTargetList


#-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-
# Other Rules
#-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-


def chooseInputForCreateBedfile(wildcards):
  graphfile = GRAPHS_DIR+f'/{wildcards.dataset}_graph.bz2'
  if Path(graphfile).exists():
    return graphfile

  if Path(f'{IN_DIR}/{wildcards.dataset}{BED_EXT}').exists():
    return f'{IN_DIR}/{wildcards.dataset}{BED_EXT}'

  return f'{IN_DIR}/{wildcards.dataset}{BEDZ_EXT}'


rule create_removed_bedfile:
  input:
    chooseInputForCreateBedfile

  output:
    IN_DIR+'/{dataset}_R'+BEDZ_EXT

  log:
    'cleanoverlapslogs/{dataset}_log.txt'

  run:
    graphfile = GRAPHS_DIR+f'/{wildcards.dataset}_graph.bz2'
    savegraph = ''
    if str(input) != graphfile:          #Input is not a graph
      savegraph = f"-g '{graphfile}'"

    shell("{EXPORT_PYTHONPATH}; "
          "{CLEANOVERLAPS} {savegraph} '{input}' '{output}' >& {log}")