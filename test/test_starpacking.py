"""
Unittests for testing the functions that compute star packings.
"""

from pathlib import Path
import random
from typing import Collection, FrozenSet, List, Set, Tuple
import unittest
import networkx as nx

from homology_evaluation.twopathcover import choose2PathCover, getFullPath
from homology_evaluation.graphfuncs import MDDSDP


class Test2PathCover(unittest.TestCase):

  def setUp(self):
    self.G = nx.Graph()


  def test_1(self):
    e = [(1,2, 4), (2,3, 3), (3,4, 5), (4,5, 3), (5,6, 4)]
    self.G.add_weighted_edges_from(e)
    removed = choose2PathCover(self.G, [2, 3, 4, 5])      #type: ignore
    self.assertEqual(removed, [(3, 4)])

  def test_2(self):
    e = [(1,2, 2), (2,3, 2), (3,4, 1), (4,5, 1), (5,6, 2), (6,7, 2)]
    self.G.add_weighted_edges_from(e)
    removed = choose2PathCover(self.G, [2, 3, 4, 5, 6])   #type: ignore
    self.assertEqual(removed, [(3, 4), (4, 5)])


  def test_3(self):
    e = [(1,2, 1), (2,3, 3), (3,4, 3), (4,5, 1)]
    self.G.add_weighted_edges_from(e)
    removed = choose2PathCover(self.G, [2, 3, 4])         #type: ignore
    self.assertEqual(removed, [(1, 2), (4, 5)])


  def test_3_stars(self):
    e = [(1,'x', 0), (1,2, 1), (2,3, 3), (3,4, 3), (4,5, 1), (5,'y', 0)]
    self.G.add_weighted_edges_from(e)
    removed = choose2PathCover(self.G, [2, 3, 4])         #type: ignore
    self.assertEqual(removed, [(1, 2), (4, 5)])


  def test_4(self):
    e = [(1,2, 1), (2,3, 1), (3,4, 3), (4,5, 3)]
    self.G.add_weighted_edges_from(e)
    removed = choose2PathCover(self.G, [2, 3, 4])         #type: ignore
    self.assertEqual(removed, [(2, 3)])


  def test_4_star(self):
    e = [(1,2, 1), (2,3, 1), (3,4, 3), (4,5, 3), (5,'x', 0)]
    self.G.add_weighted_edges_from(e)
    removed = choose2PathCover(self.G, [2, 3, 4])         #type: ignore
    self.assertEqual(removed, [(3, 4)])


  def test_5_stars(self):
    e = [(1,'x', 0), (1,2, 3), (2,3, 5), (3,4, 2), (4,5, 3), (5,6, 2),
         (6,7, 3), (7,'y', 0)]
    self.G.add_weighted_edges_from(e)
    removed = choose2PathCover(self.G, [2, 3, 4, 5, 6])   #type: ignore
    self.assertEqual(removed, [(1, 2), (3, 4), (5, 6)])


  def test_length2_2leaf(self):
    e = [(1,2, 3), (2,3, 2)]
    self.G.add_weighted_edges_from(e)
    removed = choose2PathCover(self.G, [2])               #type: ignore
    self.assertEqual(removed, [])


  def test_length2_2star(self):
    e = [(1,'x', 0), (1,2, 3), (2,3, 2), (3,'y', 0)]
    self.G.add_weighted_edges_from(e)
    removed = choose2PathCover(self.G, [2])               #type: ignore
    self.assertEqual(removed, [(2, 3)])

  def test_length2_1star1(self):
    e = [(1,'x', 0), (1,2, 3), (2,3, 2)]
    self.G.add_weighted_edges_from(e)
    removed = choose2PathCover(self.G, [2])               #type: ignore
    self.assertEqual(removed, [(2, 3)])


  def test_length2_1star2(self):
    e = [(1,2, 3), (2,3, 2), (3,'y', 0)]
    self.G.add_weighted_edges_from(e)
    removed = choose2PathCover(self.G, [2])               #type: ignore
    self.assertEqual(removed, [(2, 3)])


class TestGetFullPath(unittest.TestCase):

  def setUp(self):
    self.forest = nx.Graph()
    e = [(1,2), (2,3), (3,4), (4,5), (5,6),
         (4,7), (7,8), (8,9),
         (8,10), (10,11), (11,12)]
    self.forest.add_edges_from(e)


  def test_1(self):
    self.assertEqual(getFullPath(self.forest, [2, 3]),    #type: ignore
                     [(1,2), (2,3), (3,4)])


  def test_2(self):
    self.assertEqual(getFullPath(self.forest, [7]),       #type: ignore
                     [(4,7), (7,8)])


  def test_3(self):
    self.assertEqual(getFullPath(self.forest, [10, 11]),  #type: ignore
                     [(8,10), (10,11), (11,12)])


class TestRandomMDDS(unittest.TestCase):
  """
  Unittests for testing the functions that compute the Minimum Deletion into
  Disjoint Stars on a tree.
  """

  def test_complete(self):
    """
    Test a tree generated from a minimum spanning tree of complete graph.
    """
    reps = 1000000
    size = 15
    G = nx.complete_graph(size)
    filename = 'badtree.txt'
    for i in range(reps):
      random.seed(i)
      for u, v in G.edges():
        G.edges[u,v]['weight'] = random.randint(0, 1000)

      tree = nx.maximum_spanning_tree(G)
      dp = MDDSDP(tree)
      remset = dp.getRemovalSet()
      remsetw = sum(G.edges[u,v]['weight'] for u,v in remset)
      weight = dp.getWeight()
      if weight != remsetw:
        print(f'Weight mismatch! {weight} {remsetw}')
        print(f'  writing edges to "{filename}".')
        nx.write_weighted_edgelist(tree, filename)

      self.assertEqual(dp.getWeight(), remsetw)


class TestMDDS(unittest.TestCase):
  """
  Unittests for testing the functions that compute the Minimum Deletion into
  Disjoint Stars on a tree.
  """

  def setUp(self):
    pass


  def test_1(self):
    tree = nx.read_weighted_edgelist('test/input/tree1.txt')
    dp = MDDSDP(tree)
    self.assertEqual(dp.getWeight(), 27)
    self.assertEqual(toSets(dp.getRemovalSet()),
                     toSets([('c', 'f'), ('c', 'g'), ('b', 'e'), ('d', 'h')]))

  def test_2(self):
    tree = nx.read_weighted_edgelist('test/input/tree2.txt')
    dp = MDDSDP(tree)
    self.assertEqual(dp.getWeight(), 9)
    self.assertEqual(toSets(dp.getRemovalSet()),
                     toSets([('b', 'c'), ('b', 'e'), ('d', 'h')]))

  def test_3(self):
    tree = nx.read_weighted_edgelist('test/input/tree3.txt')
    dp = MDDSDP(tree)
    self.assertEqual(dp.getWeight(), 9)
    self.assertEqual(toSets(dp.getRemovalSet()),
                     toSets([('a', 'b'), ('b', 'd'), ('b', 'e')]))

  def test_4(self):
    """
    The same as test_3, except has a different root.
    """
    tree = nx.read_weighted_edgelist('test/input/tree4.txt')
    dp = MDDSDP(tree)
    self.assertEqual(dp.getWeight(), 9)
    self.assertEqual(toSets(dp.getRemovalSet()),
                     toSets([('a', 'b'), ('b', 'd'), ('b', 'e')]))

  def test_5(self):
    tree = nx.read_weighted_edgelist('test/input/tree5.txt')
    dp = MDDSDP(tree)
    self.assertEqual(dp.getWeight(), 8)
    self.assertEqual(toSets(dp.getRemovalSet()),
                     toSets([('c', 'b'), ('b', 'd'), ('b', 'e')]))

  def test_badtree(self):
    filename = 'badtree.txt'
    if not Path(filename).exists():
      print(f'No badtree file "{filename}".')
      return
    
    tree = nx.read_weighted_edgelist(filename)
    dp = MDDSDP(tree)
    remset = dp.getRemovalSet()
    remsetw = sum(tree.edges[u,v]['weight'] for u,v in remset)
    self.assertEqual(dp.getWeight(), remsetw)


def toSets(pairs: Collection[Tuple]) -> Set[FrozenSet]:
  """
  Convert everything to sets for comparison.
  """
  return set(frozenset(t) for t in pairs)


if __name__ == '__main__':
    unittest.main()
