Syntenic Block Evaluation
=========================

Two methods may create different syntenic blocks for the same set of genomes.  These programs are meant to compare these blocks so as to find inconsistencies between the methods.

A syntenic block (or _block_ for short) is a set of intervals from a set of genomes.  A block is a homology statement on those genomic intervals; two intervals are in the same block iff they are homologous with respect to some ancestral genome.

(__NOTE__: in our current practice, if there are multiple homologous regions in a single genome they will all appear in the same syntenic block, irrespective of distance -- in this sense the term _syntenic_ may need to be revisited).

Analysis with the script `evalblocks` proceeds in two steps, the first called _blockfiles_ and second called _graph_ (named for the input file type):

1.  blockfiles - evaluate some basic overlapping statistics about the input blocks, and optionally create a graph file (`-g`) containing the intersection graph for two blocks sets.
2.  graph - analyze the connected components of a graph file


## Creating the Intersection Graph (with _blockfiles_)

Most of the interesting information is based on the _intersection graph_. Given two sets of blocks, the intersection graph is a bipartite graph having a vertex for each... blah blah.

Say we have bed (or gff) files called `SibeliaZ-nooutgroup.bed.gz` and `SibeliaZ-nooutgroup+maf2synteny.bed.gz`. Create the intersection graph with the command

`bin/evalblocks blockfiles -g SibeliaZ-nooutgroup_VS_SibeliaZ-nooutgroup+maf2synteny_graph.bz2 data/blocks-selection/SibeliaZ-nooutgroup.bed.gz 	data/blocks-selection/SibeliaZ-nooutgroup+maf2synteny.bed.gz`.

This needs to be done once and takes some time and RAM, depending on the size of the input. To do things in parallel use the `-t` flag

`bin/evalblocks -t 30 blockfiles -g SibeliaZ-nooutgroup_VS_...`


## Evaluating the Intersection Graph (with _graph_)

With the intersection graph computed, we can analyze its connected components. For a pair of block methods that are consistent with each other we expect... blah blah.

To analyze the connected components of a graph do

`bin/evalblocks graph SibeliaZ-nooutgroup_VS_SibeliaZ-nooutgroup+maf2synteny_graph.bz2`.

This will output information about the connected components. There are various other flags that you can use to get further information.  The more pertinent are:

*   `--output-csv` will create a CSV file with information about the comparison (this is __not__ a table).
    Use `-t` threads to speed this up.
*   `--allcomponent-files` will create PDF drawings of the big (non-star) components.
*   `--allcutset-info` will create test files showing the interval overlap relations around the cutsets.


## Using Snakemake

The combination of the two command from above can be accomplished with a single snakemake command. First, be sure that the input and output directories are set to your liking in `snakeconfig/default.yaml`. You can then do the equivalent of the above two commands by specifying an output file that snakemake expects to see. With the default values for `OUT_DIR` (`block_output/`), `IN_DIR` (`data/blocks-selection`), and `GRAPHS_DIR` (`graphcache/tb`) in `snakeconfig/default.yaml` you would do

`snakemake -j 30 block_output/SibeliaZ-nooutgroup_VS_SibeliaZ-nooutgroup+maf2synteny/SibeliaZ-nooutgroup_VS_SibeliaZ-nooutgroup+maf2synteny_graph.txt`.

If you would also like to have drawings of the connected components, then do

`snakemake -j 30 block_output/SibeliaZ-nooutgroup_VS_SibeliaZ-nooutgroup+maf2synteny/components`.

Similarly, you could get the block overlap drawings for the cutsets with

`snakemake -j 30 block_output/SibeliaZ-nooutgroup_VS_SibeliaZ-nooutgroup+maf2synteny/cutsets`.

If you run snakemake without a file specified, all pairs of bed files in the directory specified by `IN_DIR` will be compared. There are several config files in `snakeconfig/` that group bed files into interesting subsets (some of these config files depend on new input directories being made -- I have symlinks in these directories).


_________________________________________________________________________
## Installation

Under Ubuntu you can install the packages using

`sudo apt install python3-numpy python3-matplotlib python3-pybedtools python3-networkx python3-prettytable python3-gffutils`

### Installing SibeliaZ

Use conda (mamba) to install `sibeliaz`.

### Installing Cactus

Use a combination of conda (mamba) and pip to install `cactus`:
0. install docker with `sudo apt install docker.io`
1. `mamba create -n cactus python=3.8 toil`
2. download and extract zip from [https://github.com/ComparativeGenomicsToolkit/cactus/releases](https://github.com/ComparativeGenomicsToolkit/cactus/releases), cd to directory
3. `python3 -m pip install -U .`
4. `python3 -m pip install -U -r ./toil-requirement.txt`
